<?php
namespace FreeMS\Repositories\Product;

use App\Models\Right;
use FreeMS\Models\User;
use FreeMS\Repositories\BaseRepository;
use Illuminate\Support\Facades\Auth;

class ProductRepository extends BaseRepository {
  protected $model;

  public function __construct($model){
    $this->model = $model;
  }

  public function getSizeOptionsByCategory($categoryId) {
    $productInventory = $this->getProductInventoryModel();
    return $productInventory::groupBy('size')->get();
  }

  public function getList($filter, $sort) {
    $selectFields = [
      'products.id as product_id',
      'products.name as product_name',
      'products.enabled',
      'products.active',
      'files.src as image',
      'MIN(product_inventories.price) as min_price, MAX(product_inventories.price) as max_price',
/*      'GROUP_CONCAT(DISTINCT(product_inventories.size) SEPARATOR " / ") as sizes',
      'GROUP_CONCAT(DISTINCT(product_colors.name) SEPARATOR " / ") as colors',*/
      'currencies.symbol as currency',
      'brands.name as brand_name',
      'SUM(product_inventories.amount) as amount'
    ];

    $products = \DB::table('products')
      ->selectRaw(implode(',', $selectFields))
      ->leftJoin('product_inventories', 'product_inventories.product_id', '=', 'products.id');

    /*$products = $products->leftJoin('product_colors', 'product_inventories.product_color_id', '=', 'products.id');*/

    $products = $products->leftJoin('currencies', 'products.currency_id', '=', 'currencies.id');

    $products = $products->leftJoin('brands', 'products.brand_id', '=', 'brands.id');

    $products->leftJoin('files', 'products.featured_image_id', '=', 'files.id');

    $products = $this->applyFilter($products, $filter);
    $products = $this->applySort($products, $sort);

    $products = $products->groupBy('products.id');

    return $products->paginate(20);
  }

  public function getProducts($filter, $shopId, $curator = false) {
    $selectFields = [
      'products.id as product_id',
      'products.name as product_name',
      'products.slug',
      'products.active',
      'files.src as image',
      'MIN(product_inventories.price) as min_price',
      'count(product_inventories.id) as optionCount',
      'GROUP_CONCAT(product_inventories.id SEPARATOR "|") as inventoryIds',
    ];

    $selectFields[] = 'currencies.symbol as currency';

    $selectFields[] = 'brands.name as brand_name';

    $products = \DB::table('products')
      ->selectRaw(implode(',', $selectFields))
      ->leftJoin('product_inventories', 'product_inventories.product_id', '=', 'products.id');

    $products->leftJoin('currencies', 'products.currency_id', '=', 'currencies.id');

    $products->leftJoin('brands', 'products.brand_id', '=', 'brands.id');

    $products->leftJoin('files', 'products.featured_image_id', '=', 'files.id');

    $products->leftJoin('products_to_categories', 'products.id', '=', 'products_to_categories.product_id');

    $products->leftJoin('shop_products', 'products.id', '=', 'shop_products.product_id');

    if($shopId) {
      $products->where('shop_products.shop_id', '=', $shopId);
    }

    if($curator) {
      $tagIds = $curator->tags->pluck('id')->toArray() ?: false;

      if($tagIds) {
        $products->leftJoin('products_to_tags', 'products.id', '=', 'products_to_tags.product_id');
        $products->whereIn('products_to_tags.product_tag_id', $tagIds);
      }
    }

    $products->where('products.active', '=', 1);

    $products->where('product_inventories.amount', '>', 0);

    if(@$filter['search']) {
      $search = $filter['search'];

      if(@$filter['suggestions']) {
        $products->where('products.name', 'REGEXP', '(^|\s)'.$search);
      } else {
        $products->where(function ($query) use ($search) {
          $query->where('products.name', 'like', '%' . $search . '%')
            ->orWhere('brands.name', 'like', '%' . $search . '%')
            ->orWhere('products.short_description', 'like', '%' . $search . '%');
        });
      }
    }

    if(@$filter['brand_id']) {
      $products->where('brands.id', '=', $filter['brand_id']);
    }

    if(@$filter['category']) {
      $products->where('products_to_categories.category_id', '=', $filter['category']);
    }

    /*if(@$filter['size']) {
      $products->where($this->getProductInventoryTableName().'.size', '=', $filter['size']);
    }*/

    if(@$filter['prices']) {
      $filter['prices'] = array_values($filter['prices']);
      $products->where(function ($q) use($filter) {
        $q->whereBetween('product_inventories.price', [$filter['prices'][0]['from'], $filter['prices'][0]['to']]);

        $l = count($filter['prices']);
        for ($i = 1; $i < $l; $i++) {
          $q->orWhereBetween('product_inventories.price', [$filter['prices'][$i]['from'], $filter['prices'][$i]['to']]);
        }
      });
    }

    /*if(@$filter['colors']) {
      $filter['colors'] = array_values($filter['colors']);
      $products->where(function ($q) use($filter) {
        $q->where($this->getProductInventoryTableName() . '.product_color_id', '=', $filter['colors'][0]);

        $l = count($filter['colors']);
        for ($i = 1; $i < $l; $i++) {
          $q->orWhere($this->getProductInventoryTableName().'.product_color_id', '=', $filter['colors'][$i]);
        }
      });
    }*/

    if(@$filter['sort']) {
      $sort = $filter['sort'];

      if($sort == 'price-asc') {
        $products->orderBy('min_price', 'ASC');
      } else if($sort == 'price-desc') {
        $products->orderBy('min_price', 'DESC');
      } else if($sort == 'date') {
        $products->orderBy($this->getTableName().'.created_at', 'DESC');
      } else {
        // TODO: sort by popularity
      }
    }

    $products = $products->groupBy('products.id');

    return $products->paginate(@$filter['page_size'] ?: 20);
  }

  public function getRelationData() {
    $data = [];

    //if($this->hasRelation($this->getProductInventoryModel(), 'color')) $data['colors'] = $this->getAllColors();
    if($this->hasRelation($this->model, 'currency')) $data['currencies'] = $this->getAllCurrencies();
    if($this->hasRelation($this->model, 'size_standard')) $data['size_standards'] = $this->getAllSizeStandards();
    if($this->hasRelation($this->model, 'weight_unit')) $data['weight_units'] = $this->getAllWeightUnits();
    if($this->hasRelation($this->model, 'brand')) $data['brands'] = $this->getAllBrands();
    if($this->hasRelation($this->model, 'shops')) $data['shops'] = $this->getAllShops();
    if($this->hasRelation($this->model, 'categories')) $data['categories'] = $this->getAllCategories();

    return $data;
  }

  public function getById($productId){
    return $this->model::findOrFail($productId);
  }

  public function createProduct($data) {
    $user = Auth::user();
    $user = User::find($user->id);

    $productInventoryModel = $this->getProductInventoryModel();

    $data['images'] = @$data['images'] ?: [];
    $images = [];
    foreach ($data['images'] as $key => $value){
      $images[$value]['ord'] = $key;
    }

    $product = new $this->model([
      'active' => $user->can(Right::PRODUCTS_MANAGE) ? @$data['active'] : 0,
      'name' => $data['name'],
      'description' => $data['description'],
      'short_description' => $data['short_description']
    ]);

    $product->user()->associate($user->id);
    $product->brand()->associate($data['brand_id']);
    $product->currency()->associate($data['currency_id']);
    $product->size_standard()->associate($data['size_standard_id']);
    $product->weight_unit()->associate($data['weight_unit_id']);

    if(@$data['featured_image_id']) $product->featured_image()->associate($data['featured_image_id']);

    $product->save();
    $product->images()->sync($images);

    $categories = @$data['categories'] ?: [];
    $product->categories()->sync($categories);

    if($user->isAdmin()) {
      $tags = @$data['tags'] ?: [];
      $product->tags()->sync($tags);

      $shops = $data['shops'] ?: [];
      $product->shops()->sync($shops);
    }

    /*$data['size'] = is_array(@$data['size']) ? $data['size'] : [];
    $i = 0;
    foreach($data['size'] as $size) {
      $inventoryData = [];
      $inventoryData['sku'] = @$data['sku'][$i];
      $inventoryData['size'] = $size;
      $inventoryData['color'] = @$data['color'][$i];
      $inventoryData['curator_price'] = @$data['curator_price'][$i];
      $inventoryData['price'] = @$data['price'][$i];
      $inventoryData['discounted_price'] = @$data['discounted_price'][$i];
      $inventoryData['weight'] = @$data['weight'][$i];
      $inventoryData['amount'] = @$data['amount'][$i];

      if($inventoryData['sku'] && $inventoryData['size'] && $inventoryData['color'] && $inventoryData['curator_price'] && $inventoryData['price']
        && $inventoryData['weight'] && $inventoryData['amount']) {
        $productInventory = new $productInventoryModel($inventoryData);
        $productInventory->color()->associate($inventoryData['color']);
        $productInventory->product()->associate($product);
        $productInventory->save();
      }
      $i++;
    }*/

    return $product;
  }

  public function updateProduct($data) {
    $user = Auth::user();
    $user = User::find($user->id);

    $product = $this->model::findOrFail($data['id']);

    $data['images'] = @$data['images'] ?: [];
    $images = [];
    foreach ($data['images'] as $key => $value){
      $images[$value]['ord'] = $key;
    }

    $product->fill([
      'active' => $user->can(Right::PRODUCTS_MANAGE) ? @$data['active'] : $product->active,
      'name' => $data['name'],
      'description' => $data['description'],
      'short_description' => $data['short_description']
    ]);

    $product->brand()->associate($data['brand_id']);
    $product->currency()->associate($data['currency_id']);
    $product->size_standard()->associate($data['size_standard_id']);
    $product->weight_unit()->associate($data['weight_unit_id']);

    if(@$data['featured_image_id']) $product->featured_image()->associate($data['featured_image_id']);

    $product->images()->sync($images);
    $product->save();

    $categories = @$data['categories'] ?: [];
    $product->categories()->sync($categories);

    if($user->isAdmin()) {
      $tags = @$data['tags'] ?: [];
      $product->tags()->sync($tags);

      $shops = @$data['shops'] ?: [];
      $product->shops()->sync($shops);
    }

    return $product;
  }

  public function deleteProduct($productId) {
    $product = $this->model::findOrFail($productId);
    return $product->delete();
  }

  protected function getAllWeightUnits() {
    $weightUnitModel = $this->getModelInstance($this->model)->weight_unit()->getRelated();
    return $weightUnitModel::all();
  }

  /*protected function getAllColors() {
    $productInventoryModel = $this->getProductInventoryModel();
    $colorModel = $this->getModelInstance($productInventoryModel)->color()->getRelated();
    return $colorModel::all();
  }*/

  protected function getAllCurrencies() {
    $currencyModel = $this->getModelInstance($this->model)->currency()->getRelated();
    return $currencyModel::all();
  }

  protected function getAllSizeStandards() {
    $sizeStandardModel = $this->getModelInstance($this->model)->size_standard()->getRelated();
    return $sizeStandardModel::all();
  }

  protected function getAllBrands() {
    $brandModel = $this->getModelInstance($this->model)->brand()->getRelated();
    return $brandModel::all();
  }

  protected function getAllShops() {
    $shopModel = $this->getModelInstance($this->model)->shops()->getRelated();
    return $shopModel::all();
  }

  protected function getAllCategories() {
    $categoryModel = $this->getModelInstance($this->model)->categories()->getRelated();
    return $categoryModel::all();
  }

  protected function getProductInventoryTableName() {
    $productInventoryModel = $this->getProductInventoryModel();
    return $this->getModelTableName($productInventoryModel);
  }

  /*protected function getColorTableName() {
    $productInventoryModel = $this->getProductInventoryModel();
    $colorModel = $this->getModelInstance($productInventoryModel)->color()->getRelated();
    return $this->getModelTableName($colorModel);
  }*/

  protected function getCurrencyTableName() {
    $currencyModel = $this->getModelInstance($this->model)->currency()->getRelated();
    return $this->getModelTableName($currencyModel);
  }

  protected function getBrandTableName() {
    $brandModel = $this->getModelInstance($this->model)->brand()->getRelated();
    return $this->getModelTableName($brandModel);
  }

  protected function getProductInventoryModel() {
    return $this->getModelInstance($this->model)->inventory()->getRelated();
  }
}