<?php
namespace FreeMS\Repositories\Category;

use FreeMS\Repositories\BaseRepository;

class CategoryRepository extends BaseRepository {
  protected $model;

  public function __construct($model){
    $this->model = $model;
  }

  public function getList($filter, $sort) {
    $brands = $this->model::whereRaw('1');

    $brands = $this->applyFilter($brands, $filter);
    $brands = $this->applySort($brands, $sort);

    return $brands->paginate(20);
  }

  public function getById($id){
    return $this->model::findOrFail($id);
  }

  public function createCategory($data) {
    $category = new $this->model([
      'name' => $data['name']
    ]);
    $category->save();

    return $category;
  }

  public function updateCategory($data) {
    $category = $this->model::findOrFail($data['id']);
    $category->fill([
      'name' => $data['name']
    ]);
    $category->save();

    return $category;
  }

  public function deleteCategory($id)
  {
    $category = $this->model::findOrFail($id);
    return $category->delete();
  }
}