<?php

namespace FreeMS\Repositories\Article;

use FreeMS\Models\Article;
use FreeMS\Models\Tag;
use Illuminate\Support\Facades\Lang;


class ArticleRepository implements ArticleRepositoryInterface
{
  public $article;

  function __construct(Article $article)
  {
    $this->article = $article;
  }

  public function getAll()
  {
    return $this->article::all();
  }

  public function getById($id)
  {
    return $this->article::find($id);
  }

  public function create($request)
  {
    $tags = $request->input('tags');
    $tagIds = [];

    if ($tags !== null) {
      foreach ($tags as $tagName) {
        $existTag = Tag::where('name_' . Lang::locale(), $tagName)->first();
        if ($existTag === null) {
          $insertedTag = new Tag ([
            'name' => $tagName
          ]);
          $insertedTag->save();
          $tagIds[] = $insertedTag->id;
        } else {
          $tagIds[] = $existTag->id;
        }
      }
    }

    $article = new Article([
      'title' => $request->input('title'),
      'text' => $request->input('text'),
    ]);
    $article->author()->associate($request->input('author'));
    $article->save();

    $article->images()->sync($request->input('images'));
    $article->categories()->sync($request->input('categories'));
    $article->tags()->sync($tagIds);

    // Save again to find related files
    $article->save();
  }

  public function update($request)
  {
    $tags = $request->input('tags');
    $tagIds = [];

    $article = $this->article->find($request->input('id'));

    if ($tags !== null) {
      foreach ($tags as $tagName) {
        $existTag = Tag::where('name_' . Lang::locale(), $tagName)->first();
        if ($existTag === null) {
          $insertedTag = new Tag ([
            'name' => $tagName
          ]);
          $insertedTag->save();
          $tagIds[] = $insertedTag->id;
        } else {
          $tagIds[] = $existTag->id;
        }
      }
    }

    $article->fill([
      'title' => $request->input('title'),
      'text' => $request->input('text'),
    ]);

    $article->author()->associate($request->input('author'));
    $article->images()->sync($request->input('images'));
    $article->categories()->sync($request->input('categories'));
    $article->tags()->sync($tagIds);

    $article->save();
  }

  public function delete($id)
  {
    return $this->article::find($id)->delete();
  }
}