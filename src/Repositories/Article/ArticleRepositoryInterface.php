<?php
namespace FreeMS\Repositories\Article;

interface ArticleRepositoryInterface {
  /* Filters, Sorts, Paginates all data */
  public function getAll();

  public function getById($id);

  public function create($data);

  public function update($data);

  public function delete($id);
}