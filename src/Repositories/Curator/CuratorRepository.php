<?php
namespace FreeMS\Repositories\Curator;

use App\Mail\CuratorInvitation;
use FreeMS\Repositories\BaseRepository;

class CuratorRepository extends BaseRepository {
  protected $model;

  public function __construct($model){
    $this->model = $model;
  }

  public function getList($filter, $sort) {
    $selectFields = [
      $this->getTableName().'.*',
      $this->getUsersTableName().'.name as name',
    ];

    $curators = \DB::table($this->getTableName())
      ->selectRaw(implode(',', $selectFields))
      ->leftJoin(''.$this->getUsersTableName().'', ''.$this->getTableName().'.user_id', '=', $this->getUsersTableName().'.id');

    $curators = $this->applyFilter($curators, $filter);
    $curators = $this->applySort($curators, $sort);

    return $curators->paginate(20);
  }

  public function getById($brandId){
    return $this->model::findOrFail($brandId);
  }

  public function createCurator($data, $user) {
    $curator = new $this->model([
      'instagram' => $data['instagram'],
      'followers' => $data['followers'],
      'about' => @$data['about'],
      'about_title' => @$data['about_title'] ?: 'A few words about me',
      'size_tops' => @$data['size_tops'],
      'size_bottoms' => @$data['size_bottoms'],
      'size_shoes' => @$data['size_shoes'],
      'size_waist' => @$data['size_waist'],
      'registration_token' => str_random(32),
      'fee' => @$data['fee']
    ]);

    $curator->user()->associate($user);
    $curator->save();

    $data['images'] = @$data['images'] ?: [];
    $images = [];
    foreach ($data['images'] as $key => $value){
      $images[$value]['ord'] = $key;
    }
    $curator->images()->sync($images);

    $tags = @$data['tags'] ?: [];
    $curator->tags()->sync($tags);

    $curator->save();

    \Mail::to($user->email)->send(new CuratorInvitation($curator));

    return $curator;
  }

  public function updateCurator($data) {
    $curator = $this->model::findOrFail($data['id']);
    $curator->fill([
      'instagram' => $data['instagram'],
      'followers' => $data['followers'],
      'about' => @$data['about'],
      'about_title' => @$data['about_title'] ?: 'A few words about me',
      'size_tops' => @$data['size_tops'],
      'size_bottoms' => @$data['size_bottoms'],
      'size_shoes' => @$data['size_shoes'],
      'size_waist' => @$data['size_waist'],
      'fee' => @$data['fee'],
    ]);

    $data['images'] = @$data['images'] ?: [];
    $images = [];
    foreach ($data['images'] as $key => $value){
      $images[$value]['ord'] = $key;
    }
    $curator->images()->sync($images);

    $tags = @$data['tags'] ?: [];
    $curator->tags()->sync($tags);

    $curator->save();

    return $curator;
  }

  public function deleteCurator($curatorId)
  {
    $curator = $this->model::findOrFail($curatorId);
    return $curator->user->delete();
  }

  protected function getUsersTableName()
  {
    $userModel = $this->getModelInstance($this->model)->user()->getRelated();
    return $this->getModelTableName($userModel);
  }
}