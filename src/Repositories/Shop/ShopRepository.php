<?php
namespace FreeMS\Repositories\Shop;

use App\Models\ProductInventory;
use FreeMS\Repositories\BaseRepository;

class ShopRepository extends BaseRepository {
  protected $model;

  public function __construct($model){
    $this->model = $model;
  }

  public function getList($filter, $sort) {
    $shops = $this->model::whereRaw('1');

    $shops = $this->applyFilter($shops, $filter);
    $shops = $this->applySort($shops, $sort);

    return $shops->paginate(20);
  }

  public function getById($shopId){
    return $this->model::findOrFail($shopId);
  }

  public function createShop($curator, $data) {
    $shop = new $this->model();
    $shop->fill([
      'name' => $curator->user->name,
      'launch_date' => @$data['launch_date']
    ]);
    $shop->curator()->associate($curator);
    $shop->save();

    return $shop;
  }

  public function updateShop($data) {
    $shop = $this->model::findOrFail($data['id']);
    $shop->fill([
      'name' => $data['name'],
      'sub_title' => $data['sub_title'],
      'cover_color' => $data['cover_color'],
      'launch_date' => $data['launch_date'],
      'theme' => $data['theme'],
    ]);

    $data['images'] = @$data['images'] ?: [];
    $images = [];
    foreach ($data['images'] as $key => $value){
      $images[$value]['ord'] = $key;
    }
    $shop->images()->sync($images);

    if(@$data['signature_id']) $shop->signature()->associate($data['signature_id']);
    $shop->save();

    return $shop;
  }
}