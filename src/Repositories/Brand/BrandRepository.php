<?php
namespace FreeMS\Repositories\Brand;

use App\Mail\BrandInvitation;
use App\Models\BrandRepresentative;
use FreeMS\Models\Role;
use FreeMS\Repositories\BaseRepository;
use Illuminate\Support\Facades\Mail;

class BrandRepository extends BaseRepository {
  protected $model;

  public function __construct($model){
    $this->model = $model;
  }

  public function getList($filter, $sort) {
    $brands = $this->model::whereRaw('1');

    $brands = $this->applyFilter($brands, $filter);
    $brands = $this->applySort($brands, $sort);

    return $brands->paginate(20);
  }

  public function getById($brandId){
    return $this->model::findOrFail($brandId);
  }

  public function createBrand($data, $user) {
    $data['images'] = @$data['images'] ?: [];
    $images = [];
    foreach ($data['images'] as $key => $value){
      $images[$value]['ord'] = $key;
    }

    $brand = new $this->model([
      'name' => $data['name'],
      'slogan' => $data['slogan'],
      'website' => $data['website'],
      'summary' => $data['summary'],
      'fee' => $data['fee'],
      'vat' => $data['vat'],
    ]);

    if(@$data['logo_id']) $brand->logo()->associate($data['logo_id']);

    $brand->save();
    $brand->images()->sync($images);

    $brandRep = new BrandRepresentative([
      'phone' => $data['rep_phone'],
      'invitation_token' => md5(uniqId())
    ]);
    $brandRep->brand()->associate($brand);
    $brandRep->user()->associate($user);
    $brandRep->save();

    Mail::to($user->email)->send(new BrandInvitation($brandRep));

    return $brand;
  }

  public function updateBrand($data) {
    $brand = $this->model::findOrFail($data['id']);
    $brand->fill([
      'name' => $data['name'],
      'slogan' => $data['slogan'],
      'website' => $data['website'],
      'summary' => $data['summary'],
      'fee' => $data['fee'],
      'vat' => $data['vat'],
    ]);

    $data['images'] = @$data['images'] ?: [];
    $images = [];
    foreach ($data['images'] as $key => $value){
      $images[$value]['ord'] = $key;
    }

    if(@$data['logo_id']) $brand->logo()->associate($data['logo_id']);

    $brand->images()->sync($images);
    $brand->save();

    $oldReps = $brand->representatives;
    $newReps = [];

    $representativeIds = @$data['representatives'] ?: [];
    foreach($representativeIds as $representativeId) {
      $representative = BrandRepresentative::where('user_id', $representativeId)->first();

      if($representative) {
        $representative->brand()->associate($brand);
      } else {
        $representative = new BrandRepresentative([
          'invitation_token' => uniqid(),
          'phone' => '',
        ]);
        $representative->user()->associate($representativeId);
        $representative->brand()->associate($brand);
        $representative->save();
      }
      
      if(!$representative->user->roles->pluck('role_key')->contains('brand_representative')) {
        $role = Role::where('role_key', 'brand_representative')->first();
        if($role) $representative->user->roles()->attach($role);
      }

      $newReps[] = $representative->id;
    }

    foreach($oldReps as $oldRep) {
      if(!in_array($oldRep->id, $newReps)) {
        $oldRep->delete();
      }
    }

    return $brand;
  }

  public function deleteBrand($brandId) {
    $brand = $this->model::findOrFail($brandId);
    return $brand->delete();
  }
}