<?php
namespace FreeMS\Repositories\User;

use App\Models\Role;
use FreeMS\Repositories\BaseRepository;

class UserRepository extends BaseRepository {
  protected $model;

  public function __construct($model){
    $this->model = $model;
  }

  public function getList($filter, $sort) {
    $users = $this->model::whereRaw('1');

    $users = $this->applyFilter($users, $filter);
    $users = $this->applySort($users, $sort);

    return $users->paginate(20);
  }

  public function getById($userId){
    return $this->model::findOrFail($userId);
  }

  public function createUser($data) {
    $user = new $this->model([
      'name' => $data['name'],
      'email' => $data['email'],
      'avatar' => @$data['avatar'],
      'password' => bcrypt($data['password']),
      'remember_token' => str_random(32)
    ]);
    $user->save();

    if(isset($data['role_key'])) {
      $role = Role::where('role_key', $data['role_key'])->first();
      if($role) $user->roles()->attach($role);
    }

    return $user;
  }

  public function updateUser($data, $user) {
    $user = $this->model::findOrFail($user->id);
    $user->fill([
      'name' => $data['name']
    ]);
    $user->save();

    return $user;
  }

  public function deleteUser($userId) {
    $user = $this->model::findOrFail($userId);
    return $user->delete();
  }
}