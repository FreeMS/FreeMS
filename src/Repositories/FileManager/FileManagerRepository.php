<?php

namespace FreeMS\Repositories\FileManager;

use App\Models\Right;
use FreeMS\Http\Requests\FileManager\CreateFile;
use FreeMS\Http\Requests\FileManager\CreateFolder;
use FreeMS\Http\Requests\FileManager\UpdateFile;
use FreeMS\Models\File;
use FreeMS\Models\Folder;
use FreeMS\Repositories\BaseRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Alaouy\Youtube\Facades\Youtube;
use Intervention\Image\ImageManagerStatic as Image;
use DB;
use Spatie\ResponseCache\Facades\ResponseCache;

class FileManagerRepository extends BaseRepository {

  private $uploadBehaviour = 'default';

  public function index(Request $request) {
    if ($request->ajax()) {
      $user = \Auth::user();

      $type = $request->input('type');
      $folderId = $request->input('folderId');

      $folder = null;
      if ($folderId) {
        // If listing specific folder
        $folder = Folder::where('id', $folderId)->first();
        $isTrash = $folder->name == '.trash';

        if ($isTrash) {
          $folders = Folder::onlyTrashed()->get();
          $files = File::onlyTrashed()->with('thumbnails');
        } else {
          $folders = $folder->children;
          $files = $folder->files()->with('thumbnails');
        }

        if ($type) $files = $files->where('type', $type);
        $files = $files->get();

        if ($isTrash) {
          foreach ($files as $file) {
            $file->src = str_replace('storage/files/', 'storage/files/trash/', $file->src);

            foreach ($file->thumbnails as $thumbnail) {
              $thumbnail->src = str_replace('storage/thumbnails/', 'storage/thumbnails/trash/', $thumbnail->src);
            }
          }
        }

        if (config('freems.self_editor_roles') && $user->can(Right::SELF_EDITOR)) {
          for ($index = 0; $index < $files->count(); $index++) {
            if (!$files[$index]->isOwnedByUser($user)) {
              $files->splice($index, 1);
              $index--;
            }
          }

          for ($index = 0; $index < $folders->count(); $index++) {
            if (!$folders[$index]->isOwnedByUser($user)) {
              $folders->splice($index, 1);
              $index--;
            }
          }
        }

        $breadcrumb = $this->getBreadcrumb($folder);
      } else {
        // If listing root folder
        $folders = Folder::where('parent_id', null)->get();
        $files = [];

        $breadcrumb = $this->getBreadcrumb();
      }

      $fileTypes = File::select('type')->groupBy('type')->get();

      if($folder) {
        $folder = $this->wrapFolder($folder);
      }

      return [
        'currentFolder' => $folder,
        'folders' => $this->wrapFolders($folders),
        'files' => $this->wrapFiles($files),
        'fileTypes' => $fileTypes,
        'breadcrumb' => $breadcrumb
      ];
    }

    // Load view without data, data will be loaded later by ajax
    return view('freems::fileManager.index');
  }

  public function store($request, $inputName = 'files') {
    $user = \Auth::user();

    $folder = null; // to return recently used folder
    $this->uploadBehaviour = $request->input('upload_behaviour');
    $uploadType = $request->input('upload_type');
    $folderId = $request->input('folder_id');
    $uploadedFiles = $request->file($inputName);

    $files = [];
    if ($uploadType == 'video') {
      $videoType = $request->input('video_type');
      $videoId = $request->input('video_id');

      if ($videoType == 'youtube') {
        $API_KEY = config('freems.youtube_api_key');
        if (!$API_KEY) throw new \Exception("Please set up youtube_api_key in config/freems.php file");
        Youtube::setApiKey($API_KEY);

        $videoInfo = Youtube::getVideoInfo($videoId);
        $useRemoteImage = (!$uploadedFiles || !count($uploadedFiles));
        $imageSrc = null;
        $extension = null;

        if ($useRemoteImage) {
          $imageSrc = $videoInfo->snippet->thumbnails->high->url;
          $extension = [];
          preg_match('#\.(\w{3,4})$#', $imageSrc, $extension);
          if ($extension && $extension[1]) $extension = $extension[1];
          else $extension = 'jpg';
        } else {
          $type = $this->getFileType($uploadedFiles[0]->getMimeType());
          if ($type != 'image') throw new \Exception("Please upload image for thumbnail");
        }

        $file = new File([
          'src' => 'not-downloaded-yet',
          'title' => $videoInfo->snippet->title,
          'caption' => $videoInfo->snippet->description,
          'width' => $videoInfo->snippet->thumbnails->high->width,
          'height' => $videoInfo->snippet->thumbnails->high->height,
          'video_id' => $videoId,
          'type' => 'youtube',
          'extension' => 'youtube',
        ]);

        DB::transaction(function () use ($file, $imageSrc, $extension, $useRemoteImage, $uploadedFiles) {
          $file->save();

          if ($useRemoteImage) {
            $filePath = 'storage/files/' . $file->slug . '.' . $extension;
            file_put_contents(public_path($filePath), file_get_contents($imageSrc));
            $file->src = $filePath;
          } else {
            $extension = strtolower($uploadedFiles[0]->getClientOriginalExtension());

            if ($extension != 'svg') {
              list($width, $height) = getimagesize($uploadedFiles[0]->getPathName());
              $file->width = $width;
              $file->height = $height;
            }

            $file->src = 'storage/' . $uploadedFiles[0]->storeAs('files', $file->slug . '.' . $extension, ['disk' => 'public']);
          }
          $file->save();
        });

        $folder = $this->placeFileToFolder($file, $folderId, 'video');
        $files = [$file];

        if (config('freems.self_editor_roles')) {
          $file->owners()->attach($user);
        }
      }
    } else {
      $uploadedFiles = $uploadedFiles ?: [];
      $result = $this->uploadFiles($uploadedFiles, $folderId);
      $folder = $result['folder'];
      $files = $result['files'];
    }

    if (config('freems.response_cache_enabled')) {
      ResponseCache::clear();
    }

    $breadcrumb = $this->getBreadcrumb($folder);

    if ($folder) {
      $folder = $this->wrapFolder($folder);
    }

    return [
      'success' => true,
      'currentFolder' => $folder,
      'folders' => [],
      'files' => $this->wrapFiles($files),
      'breadcrumb' => $breadcrumb
    ];
  }

  public function update(File $file, UpdateFile $request) {
    $user = \Auth::user();
    if (config('freems.self_editor_roles') && $user->can(Right::SELF_EDITOR)) {
      if (!$file->isOwnedByUser($user)) {
        return $file;
      }
    }

    $oldSlug = $file->slug;

    $file->fill([
      'title' => $request->input('title'),
      'caption' => $request->input('caption'),
    ]);

    $file->save();

    $cropData = json_decode($request->input('crop'));
    if ($cropData && $cropData->width && $cropData->height && $cropData->x && $cropData->y) {
      $file->width = round($cropData->width);
      $file->height = round($cropData->height);

      $image = Image::make($file->src);
      $image->crop($file->width, $file->height, round($cropData->x), round($cropData->y));
      $image->save();

      $file->save();
    }

    if ($file->slug != $oldSlug) {
      $newSrc = 'storage/files/' . $file->slug . '.' . $file->extension;
      $origSrc = storage_path(str_replace('storage/', '', $file->src));
      if (file_exists($origSrc)) {
        \File::move($origSrc, storage_path(str_replace('storage/', '', $newSrc)));
      }
      \File::move(public_path($file->src), public_path($newSrc));
      $file->src = $newSrc;
      $file->save();
    }

    if (config('freems.response_cache_enabled')) {
      ResponseCache::clear();
    }

    return $file;
  }

  public function destroy(File $file) {
    $user = \Auth::user();
    if (config('freems.self_editor_roles') && $user->can(Right::SELF_EDITOR)) {
      if (!$file->isOwnedByUser($user)) {
        return ['deleted' => false];
      }
    }

    /*if ($file->articles()->count()) {
      $message = '';
      foreach ($file->articles as $article) {
        $message .= '<a target="_blank" href="' . route('admin.articles.edit', [$article->id]) . '">' . $article->title . '</a><br>';
      }
      return ['error' => 'File is used', 'description' => $message];
    }*/

    if (config('freems.response_cache_enabled')) {
      ResponseCache::clear();
    }

    return ['deleted' => $file->delete()];
  }

  public function deleteFolder(Folder $folder) {
    $user = \Auth::user();
    if (config('freems.self_editor_roles') && $user->can(Right::SELF_EDITOR)) {
      if (!$folder->isOwnedByUser($user)) {
        return ['deleted' => false, 'error' => 'This folder can be deleted only by it\'s owners'];
      }
    }

    if ($folder->name == '.tmp' || $folder->name == '.trash') {
      throw new \Exception("Unable to delete system folder");
    }

    $files = $folder->files;
    $returnData = ['error' => 'Files is used', 'description' => '', 'deleteFolder' => true];

    foreach ($files as $file) {
      $destroyedFile = $this->destroy($file);

      if (isset($destroyedFile['error'])) {
        $returnData['deleteFolder'] = false;
        $returnData['description'] .= $destroyedFile['description'];
      }
    }

    if (config('freems.response_cache_enabled')) {
      ResponseCache::clear();
    }

    if ($returnData['deleteFolder']) {
      return ['deleted' => $folder->delete()];
    } else {
      return $returnData;
    }
  }

  public function createFolder(CreateFolder $request) {
    $user = \Auth::user();

    $folderId = $request->input('folderId');

    $name = $request->input('name');

    if ($name == '.tmp' || $name == '.trash') {
      throw new \Exception("Unable to create folder with reserved name");
    }

    $folder = new Folder([
      'name' => $name
    ]);
    $folder->save();

    if (config('freems.self_editor_roles')) {
      $folder->owners()->attach($user);
    }

    if ($folderId) {
      $parentFolder = Folder::find($folderId);
      $parentFolder->appendNode($folder);
    }

    if (config('freems.response_cache_enabled')) {
      ResponseCache::clear();
    }

    return $folder;
  }

  public function nestable() {
    $data = $_POST['sortData'];

    foreach ($data as $key => $value) {
      $fields = explode('-', $value);
      $file_id = $fields[0];
      $fileable_id = $fields[1];

      \DB::table('fileables')->where('file_id', $file_id)->where('fileable_id', $fileable_id)->update(['ord' => $key]);
    }
  }

  public function restoreFolder($id) {
    $folder = Folder::withTrashed()->find($id);

    $user = \Auth::user();
    if (config('freems.self_editor_roles') && $user->can(Right::SELF_EDITOR)) {
      if (!$folder->isOwnedByUser($user)) {
        return ['restored' => false];
      }
    }

    if ($folder && $folder->trashed()) {
      $folder->restore();
    }

    if (config('freems.response_cache_enabled')) {
      ResponseCache::clear();
    }

    return ['restored' => $folder];
  }

  public function restoreFile($id) {
    $file = File::withTrashed()->find($id);

    $user = \Auth::user();
    if (config('freems.self_editor_roles') && $user->can(Right::SELF_EDITOR)) {
      if (!$file->isOwnedByUser($user)) {
        return ['restored' => false];
      }
    }

    if ($file && $file->trashed()) {
      $file->restore();
    }

    if (config('freems.response_cache_enabled')) {
      ResponseCache::clear();
    }

    return ['restored' => $file];
  }

  public function deleteFolderForever($id) {
    $folder = Folder::withTrashed()->find($id);

    $user = \Auth::user();
    if (config('freems.self_editor_roles') && $user->can(Right::SELF_EDITOR)) {
      if (!$folder->isOwnedByUser($user)) {
        return ['deleted' => false];
      }
    }

    if ($folder && $folder->trashed()) {
      $folder->forceDelete();
    }

    return ['deleted' => true];
  }

  public function deleteFileForever($id) {
    $file = File::withTrashed()->find($id);

    $user = \Auth::user();
    if (config('freems.self_editor_roles') && $user->can(Right::SELF_EDITOR)) {
      if (!$file->isOwnedByUser($user)) {
        return ['deleted' => false];
      }
    }

    if ($file && $file->trashed()) {
      $file->forceDelete();
    }

    return ['deleted' => true];
  }

  public function downloadOriginal($id) {
    $file = File::withTrashed()->find($id);

    if (!$file) throw new \Exception("File not found");

    $user = \Auth::user();
    if (config('freems.self_editor_roles') && $user->can(Right::SELF_EDITOR)) {
      if (!$file->isOwnedByUser($user)) {
        throw new \Exception("Unauthorised");
      }
    }

    $FILE_PATH = '';

    $path = str_replace('storage/', '', $file->src);
    $path = storage_path($path);
    if (file_exists($path)) {
      $FILE_PATH = $path;
    } else if ($file->trashed()) {
      $path = str_replace('storage/files/', 'storage/files/trash/', $file->src);
      $path = public_path($path);
      if (file_exists($path)) $FILE_PATH = $path;
    } else {
      $path = public_path($file->src);
      if (file_exists($path)) $FILE_PATH = $path;
    }

    if (!$FILE_PATH) throw new \Exception("File not found");

    return Response::download($FILE_PATH);
  }

  public function removeWatermark($id) {
    $file = File::withTrashed()->find($id);

    $user = \Auth::user();
    if (config('freems.self_editor_roles') && $user->can(Right::SELF_EDITOR)) {
      if (!$file->isOwnedByUser($user)) {
        throw new \Exception("Unauthorised");
      }
    }

    if (!$file) throw new \Exception("File not found");

    File::removeWatermark($file);

    return ['success' => true];
  }

  private function placeFileToFolder($file, $folderId, $type) {
    if ($this->uploadBehaviour == 'related') {
      // Uploads in specific folder or /.tmp
      if ($folderId) {
        $folder = Folder::find($folderId);
      } else {
        $folder = Folder::where('name', '.tmp')->where('parent_id', null)->first();
      }

      // if specified folder doesn't exist or /.tmp doesn't exist
      if (!$folder) throw new \Exception($folderId ? "Specified folder not found" : ".tmp folder not found");
    } else {
      // Uploads in specific folder or /[file-type]
      if ($folderId) {
        $folder = Folder::find($folderId);
      } else {
        $folderNameByType = ucfirst($type) . 's';
        $folder = Folder::where('name', $folderNameByType)->where('parent_id', null)->first();
        if (!$folder) {
          $folder = new Folder(['name' => $folderNameByType]);
          $folder->save();
        }
      }

      // if specified folder doesn't exist or unable to create new folder by type
      if (!$folder) throw new \Exception($folderId ? "Specified folder not found" : "Unable to create folder in root");
    }

    $folder->files()->attach($file);

    return $folder;
  }

  private function uploadFiles($uploadedFiles, $folderId) {
    $user = \Auth::user();

    $folder = null;

    $files = [];

    foreach ($uploadedFiles as $uploadedFile) {
      if (!$uploadedFile->isValid()) throw new \Exception("File is not valid");

      $extension = strtolower($uploadedFile->getClientOriginalExtension());

      $originalName = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);
      $originalName = substr($originalName, 0, 32);
      $type = $this->getFileType($uploadedFile->getMimeType());

      $width = null;
      $height = null;

      if ($type == 'image' && $extension != 'svg') {
        list($width, $height) = getimagesize($uploadedFile->getPathName());
      }

      $file = new File([
        'src' => 'not-uploaded-yet',
        'title' => $originalName,
        'width' => $width,
        'height' => $height,
        'type' => $type,
        'extension' => $extension,
      ]);

      DB::transaction(function () use ($file, $uploadedFile) {
        $file->save();
        $file->src = 'storage/' . $uploadedFile->storeAs('files', $file->slug . '.' . $file->extension, ['disk' => 'public']);

        if ($file->type == 'image' && $file->extension != 'svg') {
          \File::copy(public_path($file->src), storage_path(str_replace('storage/', '', $file->src)));

          \FreeMS\Models\File::resizeTo1920($file);
          \FreeMS\Models\File::generateThumbnails($file);

          $watermark = config('freems.image_watermark');
          if ($watermark) \FreeMS\Models\File::addWatermarkOnOriginal($file, $watermark);
        }

        $file->save();
      });

      if (config('freems.self_editor_roles')) {
        $file->owners()->attach($user);
      }

      $folder = $this->placeFileToFolder($file, $folderId, $type);
      $files[] = $file;
    }

    return ['folder' => $folder, 'files' => $files];
  }

  private function getFileType($fileType) {
    $types = [
      'image/jpeg' => 'image',
      'image/gif' => 'image',
      'image/png' => 'image',
      'image/tiff' => 'image',
      'image/webp' => 'image',
      'application/msword' => 'document',
      'application/vnd.ms-excel' => 'document',
      'application/pdf' => 'document',
      'text/plain' => 'document',
      'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => 'document',
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => 'document',
      'application/xml' => 'document',
      'application/zip' => 'archive',
      'application/x-rar' => 'archive',
    ];

    if (isset($types[$fileType])) {
      return $types[$fileType];
    } else {
      throw new \Exception("File type is not supported");
    }
  }

  private function wrapFiles($files) {
    $wrappedFiles = [];
    foreach ($files as $file) {
      if ($file->thumbnails->count()) {
        $thumbnail = $file->thumbnails->last();
        $thumbnail = $thumbnail->src;
      } else {
        $thumbnail = $file->src;
      }

      $wrappedFiles[] = [
        'id' => $file->id,
        'src' => $file->src,
        'caption' => $file->caption,
        'created_at' => $file->created_at->format('d M, Y'),
        'extension' => $file->extension,
        'width' => $file->width,
        'height' => $file->height,
        'thumbnail' => $thumbnail,
        'title' => $file->title,
        'type' => $file->type,
        'video_id' => $file->video_id
      ];
    }

    return $wrappedFiles;
  }

  private function wrapFolders($folders) {
    $wrappedFolders = [];
    foreach ($folders as $folder) {
      $wrappedFolders[] = $this->wrapFolder($folder);
    }

    return $wrappedFolders;
  }

  private function wrapFolder($folder) {
    return [
      'id' => $folder->id,
      'name' => $folder->name
    ];
  }

  private function getBreadcrumb($folder = null) {
    $breadcrumb = [
      ['name' => 'All', 'id' => 0]
    ];

    if($folder) {
      $breadcrumb = array_merge($breadcrumb, $this->wrapFolders($folder->ancestors));
      $breadcrumb[] = $this->wrapFolder($folder);
    }

    // Make last child active
    $breadcrumb[count($breadcrumb) - 1]['active'] = true;

    return $breadcrumb;
  }
}