<?php
namespace FreeMS\Repositories;

use FreeMS\Models\User;
use Illuminate\Support\Facades\Auth;

abstract class BaseRepository {
  protected $sortableFields;
  protected $searchableFields;

  protected $modelInstances = [];
  protected $modelTableNames = [];

  protected $hasRelations = [];

  protected function applyFilter($query, $filter) {
    $user = Auth::user();
    $user = User::find($user->id);
    if($user->isBrandRepresentative()) {
      $query = $query->where($this->getTableName().'.user_id', $user->id);
    }

    if($filter) {
      $searchableFields = $this->getSearchableFields();
      foreach($searchableFields as $column => $criteria) {
        if(empty($searchableFields[$column])) continue;
        if(empty($filter[$column]) || !$filter[$column]) continue;

        $operator = '=';
        $value = $filter[$column];
        if($criteria['type'] == 'contains') {
          $operator = 'like';
          $value = '%'.$filter[$column].'%';
        } else if($criteria['type'] == 'starts_with') {
          $operator = 'like';
          $value = $filter[$column].'%';
        } else if($criteria['type'] == 'ends_with') {
          $operator = 'like';
          $value = '%'.$filter[$column];
        }

        $query = $query->where($criteria['field'], $operator, $value);
      }
    }

    return $query;
  }

  protected function applySort($query, $sort) {
    if(is_array($sort)) {
      $sortableFields = $this->getSortableFields();
      foreach($sort as $column => $direction) {
        if(empty($sortableFields[$column])) continue;
        $direction = $direction == 'desc' ? 'desc' : 'asc';
        $field = $sortableFields[$column];
        $query = $query->orderBy($field, $direction);
      }
    }

    return $query;
  }

  protected function getModelInstance($model) {
    if(is_string($model)) $modelName = $model;
    else $modelName = get_class($model);

    if(empty($this->modelInstances[$modelName])) $this->modelInstances[$modelName] = new $model;
    return $this->modelInstances[$modelName];
  }

  protected function getModelTableName($model) {
    if(is_string($model)) $modelName = $model;
    else $modelName = get_class($model);

    if(empty($this->modelTableNames[$modelName])) $this->modelTableNames[$modelName] = $this->getModelInstance($model)->getTable();
    return $this->modelTableNames[$modelName];
  }

  protected function getTableName() {
    return $this->getModelTableName($this->model);
  }

  protected function getSearchableFields() {
    if(!$this->searchableFields) {
      $this->searchableFields = $this->getModelInstance($this->model)->searchableFields;
    }
    return $this->searchableFields;
  }

  protected function getSortableFields() {
    if(!$this->sortableFields) {
      $this->sortableFields = $this->getModelInstance($this->model)->sortableFields;
    }
    return $this->sortableFields;
  }

  protected function hasRelation($model, $relationName) {
    if(empty($this->hasRelations[$relationName])) {
      $this->hasRelations[$relationName] = method_exists($model, $relationName);
    }
    return $this->hasRelations[$relationName];
  }
}