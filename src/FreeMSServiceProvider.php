<?php

namespace FreeMS;

use FreeMS\Http\Controllers\FileManagerController;
use FreeMS\Repositories\FileManager\FileManagerRepository;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use FreeMS\Models\File;

class FreeMSServiceProvider extends ServiceProvider
{

  /**
   * Bootstrap the application services.
   */
  public function boot()
  {
    $this->publishes([
      __DIR__ . '/config/freems.php' => config_path('freems.php'),
      __DIR__ . '/config/image-optimizer.php' => config_path('image-optimizer.php'),
    ], 'config');

    $this->publishes([
      __DIR__ . '/assets' => public_path('vendor/freems'),
    ], 'assets');

    $this->loadRoutesFrom(__DIR__ . '/routes.php');

    $this->loadMigrationsFrom(__DIR__ . '/Migrations');

    $this->loadViewsFrom(__DIR__ . '/views', 'freems');

    Schema::defaultStringLength(191);
  }

  /**
   * Register the application services.
   */
  public function register()
  {
    $this->app->bind('FreeMS\Repositories\Article\ArticleRepositoryInterface', 'FreeMS\Repositories\Article\ArticleRepository');

    $this->app->when(FileManagerController::class)
      ->needs(FileManagerRepository::class)
      ->give(function () {
        return new FileManagerRepository();
      });
  }
}