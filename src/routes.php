<?php

if(config('freems.localization')) {
  $routing_config = [
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => [ 'web', 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
  ];
} else {
  $routing_config = [
    'middleware' => ['web']
  ];
}

Route::group($routing_config, function () {

  $middlewares = ['auth'];

  if(config('freems.response_cache_enabled')) {
    $middlewares[] = 'doNotCacheResponse';
  }

  Route::group(['middleware' => $middlewares, 'prefix' => config('freems.admin_path')], function () {

    Route::get('/dashboard', 'FreeMS\Http\Controllers\DashboardController@index')->name('freems.dashboard');

    Route::post('/files/createFolder/', 'FreeMS\Http\Controllers\FileManagerController@createFolder')->name('freems.file.createFolder');
    Route::delete('/files/deleteFolder/{folder}', 'FreeMS\Http\Controllers\FileManagerController@deleteFolder')->name('freems.file.deleteFolder');
    Route::post('/files/restoreFolder/{id}', 'FreeMS\Http\Controllers\FileManagerController@restoreFolder')->name('freems.file.restoreFolder');
    Route::post('/files/restoreFile/{id}', 'FreeMS\Http\Controllers\FileManagerController@restoreFile')->name('freems.file.restoreFile');
    Route::delete('/files/deleteFolderForever/{id}', 'FreeMS\Http\Controllers\FileManagerController@deleteFolderForever')->name('freems.file.deleteFolderForever');
    Route::delete('/files/deleteFileForever/{id}', 'FreeMS\Http\Controllers\FileManagerController@deleteFileForever')->name('freems.file.deleteFileForever');
    Route::get('/files/downloadOriginal/{id}', 'FreeMS\Http\Controllers\FileManagerController@downloadOriginal')->name('freems.file.downloadOriginal');
    Route::post('/files/removeWatermark/{id}', 'FreeMS\Http\Controllers\FileManagerController@removeWatermark')->name('freems.file.removeWatermark');

    Route::resource('/files', 'FreeMS\Http\Controllers\FileManagerController', [
      'names' => [
        'index' => 'freems.file.index',
        'store' => 'freems.file.store',
        'update' => 'freems.file.update',
        'destroy' => 'freems.file.delete'
      ]
    ]);

    Route::post('/files/sortImages/', 'FreeMS\Http\Controllers\FileManagerController@nestable')->name('freems.file.sort');

  });

});