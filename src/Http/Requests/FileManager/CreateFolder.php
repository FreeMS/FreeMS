<?php

namespace FreeMS\Http\Requests\FileManager;

use Illuminate\Foundation\Http\FormRequest;

class CreateFolder extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'name' => 'string|required|max:255',
      'folderId' => 'integer|nullable|exists:folders,id'
    ];
  }
}
