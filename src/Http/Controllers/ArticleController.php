<?php

namespace FreeMS\Http\Controllers;

use Illuminate\Routing\Controller;

class ArticleController extends Controller
{
  public function index() {
    return view('freems::articles.index');
  }
}
