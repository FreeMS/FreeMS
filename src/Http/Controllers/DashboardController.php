<?php

namespace FreeMS\Http\Controllers;

use Illuminate\Routing\Controller;

class DashboardController extends Controller
{
  public function index() {
    return view('freems::dashboard.index');
  }
}
