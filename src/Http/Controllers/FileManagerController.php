<?php

namespace FreeMS\Http\Controllers;

use FreeMS\Repositories\FileManager\FileManagerRepository;
use Illuminate\Routing\Controller;
use FreeMS\Http\Requests\FileManager\CreateFolder;
use FreeMS\Models\Folder;
use FreeMS\Models\File;
use Auth;
use Illuminate\Http\Request;
use FreeMS\Http\Requests\FileManager\CreateFile;
use FreeMS\Http\Requests\FileManager\UpdateFile;
use DB;

class FileManagerController extends Controller
{
  protected $fileManagerRepository;

  public function __construct(FileManagerRepository $fileManagerRepository){
    $this->fileManagerRepository = $fileManagerRepository;
  }

  public function index(Request $request){
    return $this->fileManagerRepository->index($request);
  }

  public function store(CreateFile $request){
    return $this->fileManagerRepository->store($request);
  }

  public function update(File $file, UpdateFile $request){
    return $this->fileManagerRepository->update($file, $request);
  }

  public function destroy(File $file){
    return $this->fileManagerRepository->destroy($file);
  }

  public function deleteFolder(Folder $folder)
  {
    return $this->fileManagerRepository->deleteFolder($folder);
  }

  public function createFolder(CreateFolder $request)
  {
    return $this->fileManagerRepository->createFolder($request);
  }

  public function nestable(){
    return $this->fileManagerRepository->nestable();
  }

  public function restoreFolder($id){
    return $this->fileManagerRepository->restoreFolder($id);
  }

  public function restoreFile($id){
    return $this->fileManagerRepository->restoreFile($id);
  }

  public function deleteFolderForever($id){
    return $this->fileManagerRepository->deleteFolderForever($id);
  }

  public function deleteFileForever($id){
    return $this->fileManagerRepository->deleteFileForever($id);
  }

  public function downloadOriginal($id){
    return $this->fileManagerRepository->downloadOriginal($id);
  }

  public function removeWatermark($id){
    return $this->fileManagerRepository->removeWatermark($id);
  }
}
