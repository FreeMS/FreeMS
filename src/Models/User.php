<?php

namespace FreeMS\Models;

use App\Models\BrandRepresentative;
use App\Models\Curator;
use App\Models\Notification;
use App\Models\Order;
use App\Models\RemoteShopApi;
use App\Models\ShippingAddress;
use App\Models\SubOrder;
use Illuminate\Notifications\Notifiable;
use FreeMS\Models\Traits\ManagesUserRightsAndRolesTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
  use Notifiable, ManagesUserRightsAndRolesTrait;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name', 'email', 'password', 'avatar', 'gender', 'birth_date', 'is_guest', 'sign_up_token'
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password', 'remember_token',
  ];

  protected $with = [
    'rights', 'roles'
  ];

  public function isBrandRepresentative() {
    return $this->roles->pluck('role_key')->contains('brand_representative');
  }

  public function isCurator() {
    if($this->roles->pluck('role_key')->contains('curator') || $this->roles->pluck('role_key')->contains('affiliator')) return true;

    if($this->curator->count()) return true;

    return false;
  }

  public function isAffiliator() {
    return $this->roles->pluck('role_key')->contains('affiliator');
  }

  public function isAdmin() {
    return $this->roles->pluck('role_key')->contains('administrator');
  }

  public function rights()
  {
    /** @var \Illuminate\Database\Eloquent\Relations\BelongsToMany $relation */
    $relation = $this->belongsToMany(Right::class, 'user_rights', 'user_id', 'right_id');
    return $relation->withTimestamps();
  }

  public function roles()
  {
    /** @var \Illuminate\Database\Eloquent\Relations\BelongsToMany $relation */
    $relation = $this->belongsToMany(Role::class, 'user_roles', 'user_id', 'role_id');
    return $relation->withTimestamps();
  }

  public function shipping_addresses()
  {
    return $this->hasMany(ShippingAddress::class, 'user_id');
  }

  public function orders()
  {
    return $this->hasMany(Order::class, 'user_id');
  }

  public function subOrders()
  {
    return $this->hasMany(SubOrder::class, 'user_id');
  }

  public function curator()
  {
    return $this->hasOne(Curator::class, 'user_id');
  }

  public function brand_representatives()
  {
    return $this->hasMany(BrandRepresentative::class, 'user_id');
  }

  public function following()
  {
    return $this->belongsToMany(Curator::class, 'curator_followers', 'user_id', 'curator_id')->withTimestamps();
  }

  public function shop_apis()
  {
    return $this->hasMany(RemoteShopApi::class, 'user_id');
  }
}