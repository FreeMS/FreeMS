<?php

namespace FreeMS\Models;

use Illuminate\Database\Eloquent\Model;

class Right extends Model
{
  const USERS_VIEW = 'users.view';
  const USERS_MANAGE = 'users.manage';

  public function users() {
    return $this->belongsToMany(User::class, 'user_rights', 'right_id', 'user_id');
  }
}
