<?php
/**
 * Created by PhpStorm.
 * User: Hertz
 * Date: 2016-05-26
 * Time: 18:35
 */

namespace FreeMS\Models;

use Illuminate\Database\Eloquent\Model;


abstract class LocalizedModel extends Model
{

  protected $localeSuffixed = [

  ];

  /**
   * {@inheritdoc}
   */
  public function hasGetMutator($key)
  {
    return $this->isAttributeLocaleSuffixed($key) || parent::hasGetMutator($key);
  }

  /**
   * @param $key
   * @return bool
   */
  protected function isAttributeLocaleSuffixed($key)
  {
    if (property_exists($this, 'localeSuffixed')) {
      return in_array($key, $this->{'localeSuffixed'});
    }
    return false;
  }

  /**
   * {@inheritdoc}
   */
  public function mutateAttribute($key, $value)
  {
    if ($this->isAttributeLocaleSuffixed($key)) {
      return $this->mutateLocaleSuffixedAccessor($key);
    }
    return parent::mutateAttribute($key, $value);
  }

  public function hasSetMutator($key)
  {
    return $this->isAttributeLocaleSuffixed($key) || parent::hasSetMutator($key);
  }

  /**
   * {@inheritdoc}
   */
  public function setAttribute($key, $value)
  {
    // First we will check for the presence of a mutator for the set operation
    // which simply lets the developers tweak the attribute as it is set on
    // the model, such as "json_encoding" an listing of data for storage.
    if ($this->hasSetMutator($key)) {
      if ($this->isAttributeLocaleSuffixed($key)) {
        return $this->mutateLocaleSuffixedMutator($key, $value);
      }

      $method = 'set' . Str::studly($key) . 'Attribute';

      return $this->{$method}($value);
    }

    // If an attribute is listed as a "date", we'll convert it from a DateTime
    // instance into a form proper for storage on the database tables using
    // the connection grammar's date format. We will auto set the values.
    elseif ($value && (in_array($key, $this->getDates()) || $this->isDateCastable($key))) {
      $value = $this->fromDateTime($value);
    }

    if ($this->isJsonCastable($key) && !is_null($value)) {
      $value = $this->asJson($value);
    }

    $this->attributes[$key] = $value;

    return $this;
  }

  protected function mutateLocaleSuffixedAccessor($key)
  {
    $localizedAttributeName = $key . '_' . \App::getLocale();
    return $this->getAttributeFromArray($localizedAttributeName);
  }

  protected function mutateLocaleSuffixedMutator($key, $value)
  {
    $localizedAttributeName = $key . '_' . \App::getLocale();
    $this->attributes[$localizedAttributeName] = $value;

    /* Set value in other langs if they are empty */
    if(isset($this->nonTranslatableFields) && in_array($key, $this->nonTranslatableFields)){
      // These attribute values are not translatable
    } else {
      $locales = config('app.locales');
      foreach ($locales as $locale) {
        if ($locale == \App::getLocale()) continue;

        $localizedAttributeName = $key . '_' . $locale;
        if (empty($this->attributes[$localizedAttributeName]) || !$this->attributes[$localizedAttributeName]) {
          $this->attributes[$localizedAttributeName] = $value;
        }
      }
    }

    return $value;
  }

  public function getLocalizedTitle($locale) {
    $activeLocale = $this->getActiveLocale($locale);
    return $this->{'title_'.$activeLocale};
  }

  public function getLocalizedSlug($locale) {
    $activeLocale = $this->getActiveLocale($locale);
    return $this->{'slug_'.$activeLocale};
  }

  public function getActiveLocale($preferredLocale = null) {
    if($preferredLocale && $this->{'active_'.$preferredLocale}) {
      return $preferredLocale;
    }

    $locales = config('app.locales');
    $enIndex  = array_search('en', $locales);
    $spliced = array_splice($locales, $enIndex, 1);
    array_unshift($locales, $spliced[0]);

    foreach($locales as $loc) {
      if($this->{'active_'.$loc}) {
        return $loc;
      }
    }

    return 'ka';
  }
}