<?php

namespace FreeMS\Models;

use Illuminate\Database\Eloquent\Model;

abstract class ImagableModel extends Model
{
  protected static function buildRelationsToFiles($item, $parentFolderName, $folderName, $relationType = 'multi', $relationName = 'image') {
    $item->load('folders');
    if($relationType == 'single') {
      $item->load($relationName);
      $relatedFiles = [$item->$relationName];
    } else {
      $item->load($relationName.'s');
      $relatedFiles = $item->{$relationName.'s'};
    }

    if(count($relatedFiles)) {
      $folder = self::getFolder($item, $parentFolderName, $folderName);
      $tmpFolder = Folder::where('name', '.tmp')->where('parent_id', null)->first();

      foreach($relatedFiles as $relatedFile) {
        if(!$folder->files->contains($relatedFile)) {
          $folder->files()->attach($relatedFile);
        }

        if($tmpFolder && $tmpFolder->files->contains($relatedFile)) {
          $tmpFolder->files()->detach($relatedFile);
        }
      }
    }
  }

  protected static function getFolder($item, $parentFolderName, $folderName){
    $user = \Auth::user();

    if($item->folders->count()) {
      return $item->folders[0];
    } else {
      $itemsFolder = Folder::where('name', $parentFolderName)->where('parent_id', null)->first();
      if(!$itemsFolder) {
        $itemsFolder = new Folder(['name' => $parentFolderName]);
        $itemsFolder->save();
      }

      $folder = new Folder(['name' => $folderName]);
      $itemsFolder->appendNode($folder);
      $folder->save();
      if(config('freems.self_editor_roles')) {
        $folder->owners()->attach($user);
      }
      $item->folders()->attach($folder);
      return $folder;
    }
  }

  public function renderImage($cfg)
  {
    $styles = $cfg['styles'] ?? [];
    $class = $cfg['class'] ?? false;
    $maxSize = $cfg['maxSize'] ?? 1920;
    $minSize = $cfg['minSize'] ?? 360;
    $defaultImageSize = $cfg['defaultImageSize'] ?? 'large';
    $i = $cfg['i'] ?? 0;
    $returnOutput = $cfg['returnOutput'] ?? false;

    $result = null;

    if($this->images === null) {
      if($this->image) $images = [ $this->image ];
      else $images = [];
    } else {
      $images = $this->images;
    }

    foreach($images as $image) {
      if($image->type == 'image') {
        $result = $image;
        break;
      }
    }

    if($result) {
      $output = $result->render($styles, $class, $maxSize, $minSize);
      if($returnOutput) return $output;
      else echo $output;
    } else {
      $default_images = config('freems.default_images')[$defaultImageSize];
      $index = ($i % count($default_images));
      $src = $default_images[$index];

      $file = new File([
        'src' => $src,
        'title' => $this->title,
        'type' => 'image',
      ]);

      $output = $file->render($styles, $class, $maxSize, $minSize);
      if($returnOutput) return $output;
      else echo $output;
    }
  }

  public function getFullImageSrcAttribute() {
    if($this->images === null) {
      if($this->image) $images = [ $this->image ];
      else $images = [];
    } else {
      $images = $this->images;
    }

    foreach($images as $image) {
      if($image->type == 'image') {
        return \URL::to('/').'/'.$image->src;
      }
    }

    $default_images = config('freems.default_images')['large'];
    return \URL::to('/').'/'.$default_images[0];
  }

  public function getThumbnailImageSrcAttribute() {
    if($this->images === null) {
      if($this->image) $images = [ $this->image ];
      else $images = [];
    } else {
      $images = $this->images;
    }

    foreach($images as $image) {
      if($image->type == 'image') {
        foreach($image->thumbnails as $thumbnail) $image = $thumbnail;
        return \URL::to('/').'/'.$image->src;
      }
    }

    $default_images = config('freems.default_images')['large'];
    return \URL::to('/').'/'.$default_images[0];
  }
}
