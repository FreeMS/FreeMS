<?php
namespace FreeMS\Models\Traits;

trait ActivityLogConfigurator
{
  /* Only relation ids */
  protected static $logAttributes = [];

  protected static $logOnlyDirty = false;

  protected static $logName = 'system';

  public function __construct(array $attributes = array())
  {
    parent::__construct($attributes);

    self::$logName = str_replace('App\\Models\\', '', get_class($this));

    $locales = config('app.locales');

    if(property_exists($this, 'localeSuffixed')) {
      foreach ($this->localeSuffixed as $field) {
        foreach ($locales as $locale) {
          self::$logAttributes[] = $field.'_'.$locale;
        }
      }
    }

    if(property_exists($this, 'fillable')) {
      foreach ($this->fillable as $field) {
        if(property_exists($this, 'localeSuffixed') && in_array($field, $this->localeSuffixed)) continue;
        self::$logAttributes[] = $field;
      }
    }

    if(property_exists($this, 'loggable')) {
      foreach ($this->loggable as $field) {
        self::$logAttributes[] = $field;
      }
    }
  }
}
