<?php
namespace FreeMS\Models\Traits;

trait SavesState
{
  /* Only relation ids */
  private $savedState = [];

  private $saveStateAttributes = [];

  public function __construct(array $attributes = array())
  {
    parent::__construct($attributes);

    $locales = config('app.locales');

    if(property_exists($this, 'localeSuffixed')) {
      foreach ($this->localeSuffixed as $field) {
        foreach ($locales as $locale) {
          $this->saveStateAttributes[] = $field.'_'.$locale;
        }
      }
    }

    if(property_exists($this, 'fillable')) {
      foreach ($this->fillable as $field) {
        if(property_exists($this, 'localeSuffixed') && in_array($field, $this->localeSuffixed)) continue;
        $this->saveStateAttributes[] = $field;
      }
    }

    if(property_exists($this, 'loggable')) {
      foreach ($this->loggable as $field) {
        $this->saveStateAttributes[] = $field;
      }
    }
  }

  public function getAllAttributes() {
    $data = [];

    foreach($this->saveStateAttributes as $attribute) {
      $relatedAttribute = null;
      if (str_contains($attribute, '.')) {
        list($relatedModelName, $relatedAttribute) = explode('.', $attribute);
      }

      if($relatedAttribute) {
        $this->load($relatedModelName);

        if(is_a($this->$relatedModelName, 'Illuminate\Database\Eloquent\Collection')) {
          $data[$attribute] = $this->$relatedModelName ? $this->$relatedModelName->pluck($relatedAttribute)->implode('|') : '';
        } else {
          $data[$attribute] = $this->$relatedModelName->$relatedAttribute ?? '';
        }
      } else {
        $data[$attribute] = $this->$attribute;
      }
    }

    return $data;
  }

  public function saveState() {
    $this->savedState = $this->getAllAttributes();
  }

  public function getModifiedAttributes() {
    $currentState = $this->getAllAttributes();
    $modifiedKeys = array_keys(array_diff($currentState, $this->savedState));
    $result = [
      'old' => [],
      'new' => []
    ];

    foreach($modifiedKeys as $modifiedKey) {
      $result['old'][$modifiedKey] = $this->savedState[$modifiedKey] ?? null;
      $result['new'][$modifiedKey] = $currentState[$modifiedKey] ?? null;
    }

    return count($modifiedKeys) ? $result : null;
  }
}
