<?php

namespace FreeMS\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Kalnoy\Nestedset\NodeTrait;

class Folder extends Model
{
  use NodeTrait, SoftDeletes;

  protected $dates = ['deleted_at'];

  protected $fillable = [
    'name'
  ];

  public function articles()
  {
    return $this->morphedByMany(Article::class, 'folderable');
  }

  public function destinations()
  {
    return $this->morphedByMany(Destination::class, 'folderable');
  }

  public function files()
  {
    return $this->morphedByMany(File::class, 'folderable');
  }

  public function delete()
  {
    $folders = $this->children()->withTrashed()->get();
    $files = $this->files;

    foreach($folders as $folder) {
      if(!$folder->trashed()) $folder->delete();
    }

    foreach($files as $file) {
      if(!$file->trashed()) $file->delete();
    }

    return parent::delete();
  }

  public function forceDelete()
  {
    $folders = $this->children()->withTrashed()->get();
    $files = $this->files()->withTrashed()->get();

    foreach($folders as $folder) {
      $folder->forceDelete();
    }

    foreach($files as $file) {
      $file->forceDelete();
    }

    DB::table('folders')->where('id', $this->id)->delete();

    return true;
  }

  public function restore()
  {
    $folders = $this->children()->withTrashed()->get();
    $files = $this->files()->withTrashed()->get();

    foreach($folders as $folder) {
      $folder->restore();
    }

    foreach($files as $file) {
      $file->restore();
    }

    return parent::restore();
  }

  public function owners()
  {
    return $this->morphToMany(User::class, 'ownable');
  }

  public function isOwnedByUser($user)
  {
    return $this->owners->pluck('id')->contains($user->id);
  }
}
