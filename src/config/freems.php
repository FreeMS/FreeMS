<?php

return [

  /**
   * URL path to admin panel. For example, freems refers to http://yourdomain.com/freems/
   */

  'admin_path' => 'freems',

  'youtube_api_key' => 'AIzaSyBp7q0vuSsOGaVaUzjuAattIcASG2xmb1A',

  'localization' => false,

  'self_editor_roles' => false,

  'image_watermark' => false, // 'img/watermark.png'

  /**
   'large' => [
      'img/default-image-1.svg',
      'img/default-image-2.svg',
      'img/default-image-3.svg',
      'img/default-image-4.svg',
      'img/default-image-5.svg',
      'img/default-image-6.svg',
    ],
    'small' => [
      'img/default-image-small-1.svg',
      'img/default-image-small-2.svg',
      'img/default-image-small-3.svg',
      'img/default-image-small-4.svg',
      'img/default-image-small-5.svg',
      'img/default-image-small-6.svg',
    ]
   */
  'default_images' => [],

  'response_cache_enabled' => false,

];
