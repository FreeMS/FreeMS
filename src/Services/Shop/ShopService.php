<?php
namespace FreeMS\Services\Shop;

use App\Models\Brand;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductColor;
use App\Models\ProductInventory;
use App\Models\ProductInventoryOption;
use App\Services\ProductService;
use FreeMS\Repositories\Product\ProductRepository;
use FreeMS\Repositories\Shop\ShopRepository;

class ShopService {
  protected $repository;
  protected $productService;

  public function __construct(ShopRepository $repository, ProductService $productService){
    $this->repository = $repository;
    $this->productService = $productService;
  }

  public function getList($query = null) {
    $filter = null;
    $sort = null;

    if(isset($query['filter'])) $filter = $query['filter'];
    if(isset($query['sort'])) $sort = $query['sort'];

    $result = $this->repository->getList($filter, $sort);

    return $result;
  }

  public function getById($shopId){
    return $this->repository->getById($shopId);
  }

  public function createShop($curator, $data) {
    $this->repository->createShop($curator, $data);
  }

  public function updateShop($data) {
    $this->repository->updateShop($data);
  }

  public function getFilter($currentFilter = []) {
    $productRepository = new ProductRepository(Product::class);

    $categories = ProductCategory::defaultOrder()->get()->toTree();

    $colors = [];
    $sizes = []; //ProductInventoryOption::where('name', 'Size')->get();

    $priceRanges = [
      (object) [ 'name' => '0 - 50', 'id' => 1 ],
      (object) [ 'name' => '51 - 250', 'id' => 2 ],
      (object) [ 'name' => '251 - 500', 'id' => 3 ],
      (object) [ 'name' => '500 +', 'id' => 4 ],
    ];

    $currentFilter['search'] = @$currentFilter['search'] ?: null;
    $currentFilter['category'] = @$currentFilter['category'] ?: null;
    $currentFilter['brand'] = @$currentFilter['brand'] ?: null;
    $currentFilter['size'] = @$currentFilter['size'] ?: null;
    $currentFilter['colors'] = @$currentFilter['colors'] ?: [];
    $currentFilter['prices'] = @$currentFilter['prices'] ?: [];
    $currentFilter['sort'] = @$currentFilter['sort'] ?: null;

    $brands = Brand::all();

    //$sizes = $productRepository->getSizeOptionsByCategory($currentFilter['category']);

    $sortOptions = [
      (object) [ 'name' => 'Newest', 'value' => 'date' ],
      (object) [ 'name' => 'Price low to high', 'value' => 'price-asc' ],
      (object) [ 'name' => 'Price high to low', 'value' => 'price-desc' ],
      (object) [ 'name' => 'Popular', 'value' => 'popular' ],
    ];

    $filter = [
      'search' => [],
      'categories' => [],
      'brands' => [],
      'sizes' => [],
      'colors' => [],
      'prices' => [],
      'sort' => []
    ];

    if($currentFilter['category']) {
      $tmpQuery = $currentFilter;
      $tmpQuery['category'] = null;
      $filter['categories'][] = (object)['id' => 0, 'name' => 'All', 'level' => 1, 'query_string' => http_build_query($tmpQuery), 'children' => []];
    }

    $traverse = function ($categories, $prefix = '') use (&$traverse, &$currentFilter, &$filter) {
      foreach ($categories as $category) {
        $tmpQuery = $currentFilter;
        $tmpQuery['category'] = $category->id;
        $category->query_string = http_build_query($tmpQuery);
        $category->level = !$prefix ? 1 : 2;
        $filter['categories'][] = $category;
        $traverse($category->children, $prefix.'-');
      }
    };
    $traverse($categories);

    if($currentFilter['brand']) {
      $tmpQuery = $currentFilter;
      $tmpQuery['brand'] = null;
      $filter['brands'][] = (object) [ 'id' => 0, 'name' => 'All', 'query_string' => http_build_query($tmpQuery) ];
    }
    foreach($brands as $brand) {
      $tmpQuery = $currentFilter;
      $tmpQuery['brand'] = $brand->id;
      $brand->query_string = http_build_query($tmpQuery);
      $filter['brands'][] = $brand;
    }
    
    if($currentFilter['size']) {
      $tmpQuery = $currentFilter;
      $tmpQuery['size'] = null;
      $filter['sizes'][] = (object) [ 'size' => 'All', 'query_string' => http_build_query($tmpQuery) ];
    }
    foreach($sizes as $size) {
      $tmpQuery = $currentFilter;
      $tmpQuery['size'] = $size->size;
      $size->query_string = http_build_query($tmpQuery);
      $filter['sizes'][] = $size;
    }

    foreach($priceRanges as $price) {
      $price->query_string = $this->getQueryBuilderByItem($currentFilter, $price->id, 'prices');
      $filter['prices'][] = $price;
    }

    foreach($colors as $color) {
      $color->query_string = $this->getQueryBuilderByItem($currentFilter, $color->id, 'colors');
      $filter['colors'][] = $color;
    }

    foreach($sortOptions as $sortOption) {
      $tmpQuery = $currentFilter;
      $tmpQuery['sort'] = $sortOption->value;
      $sortOption->query_string = http_build_query($tmpQuery);
      $filter['sort'][] = $sortOption;
    }

    $tmpQuery = $currentFilter;
    unset($tmpQuery['search']);
    $filter['search'] = [
      'value' => $currentFilter['search'],
      'query_string' => http_build_query($tmpQuery)
    ];

    return $filter;
  }

  public function getFilteredProducts($filter = [], $shopId, $curator = false)
  {
    $productRepository = new ProductRepository(Product::class);

    if(isset($filter['brand'])) $filter['brand_id'] = $filter['brand'];

    $filter['search'] = @$filter['search'] ?: null;
    $filter['category'] = @$filter['category'] ?: null;
    $filter['size'] = @$filter['size'] ?: null;
    $filter['colors'] = @$filter['colors'] ?: [];
    $filter['prices'] = @$filter['prices'] ?: [];
    $currentFilter['sort'] = @$filter['sort'] ?: null;
    $currentFilter['brand_id'] = @$filter['brand_id'] ?: null;

    foreach($filter['prices'] as $k => $v) {
      if($v == 1) {
        $filter['prices'][$k] = [ 'from' => 0, 'to' => 50 ];
      } else if($v == 2) {
        $filter['prices'][$k] = [ 'from' => 51, 'to' => 250 ];
      } else if($v == 3) {
        $filter['prices'][$k] = [ 'from' => 251, 'to' => 500 ];
      } else if($v == 4) {
        $filter['prices'][$k] = [ 'from' => 501, 'to' => 1000000 ];
      }
    }

    $products = $productRepository->getProducts($filter, $shopId, $curator);

    foreach($products as &$item) {
      $product = Product::find($item->product_id);
      $item->options = $this->productService->extractOptions($product);
      $item->images = $product->images->pluck('src')->toArray();
    }

    return $products;
  }

  private function getQueryBuilderByItem($currentFilter, $id, $item) {
    $tmpQuery = $currentFilter;
    if(in_array($id, $tmpQuery[$item])) {
      $key = array_search($id, $tmpQuery[$item]);
      unset($tmpQuery[$item][$key]);
    } else {
      $tmpQuery[$item][] = $id;
    }

    return http_build_query($tmpQuery);
  }
}