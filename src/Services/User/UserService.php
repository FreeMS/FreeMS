<?php
namespace FreeMS\Services\User;

use FreeMS\Repositories\User\UserRepository;

class UserService {
  protected $repository;

  public function __construct(UserRepository $repository){
    $this->repository = $repository;
  }

  public function getList($query = null) {
    $filter = null;
    $sort = null;

    if(isset($query['filter'])) $filter = $query['filter'];
    if(isset($query['sort'])) $sort = $query['sort'];

    $result = $this->repository->getList($filter, $sort);

    return $result;
  }

  public function getById($userId){
    return $this->repository->getById($userId);
  }

  public function createUser($data) {
    return $this->repository->createUser($data);
  }

  public function updateUser($data, $user) {
    $this->repository->updateUser($data, $user);
  }

  public function deleteUser($userId) {
    return $this->repository->deleteUser($userId);
  }
}