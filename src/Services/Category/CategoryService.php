<?php
namespace FreeMS\Services\Category;

use FreeMS\Repositories\Category\CategoryRepository;

class CategoryService {
  protected $repository;

  public function __construct(CategoryRepository $repository){
    $this->repository = $repository;
  }

  public function getList($query = null) {
    $filter = null;
    $sort = null;

    if(isset($query['filter'])) $filter = $query['filter'];
    if(isset($query['sort'])) $sort = $query['sort'];

    $result = $this->repository->getList($filter, $sort);

    return $result;
  }

  public function getById($id){
    return $this->repository->getById($id);
  }

  public function createCategory($data) {
    return $this->repository->createCategory($data);;
  }

  public function updateCategory($data) {
    return $this->repository->updateCategory($data);
  }

  public function deleteCategory($id) {
    $this->repository->deleteCategory($id);
  }
}