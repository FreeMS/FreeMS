<?php
namespace FreeMS\Services\Curator;

use App\Models\Role;
use FreeMS\Repositories\Curator\CuratorRepository;
use FreeMS\Services\Shop\ShopService;
use FreeMS\Services\User\UserService;

class CuratorService {
  protected $repository;
  protected $userService;
  protected $shopService;

  public function __construct(CuratorRepository $repository, UserService $userService, ShopService $shopService){
    $this->repository = $repository;
    $this->userService = $userService;
    $this->shopService = $shopService;
  }

  public function getList($query = null) {
    $filter = null;
    $sort = null;

    if(isset($query['filter'])) $filter = $query['filter'];
    if(isset($query['sort'])) $sort = $query['sort'];

    $result = $this->repository->getList($filter, $sort);

    return $result;
  }

  public function getById($brandId){
    return $this->repository->getById($brandId);
  }

  public function createCurator($data, $affiliator = null) {
    $data['role_key'] = $affiliator ? 'affiliator' : 'curator';
    $user = $this->userService->createUser($data);
    $curator = $this->repository->createCurator($data, $user);
    if($affiliator) {
      $curator->affiliate()->associate($affiliator);
      $curator->save();
    } else {
      $this->shopService->createShop($curator, $data);
    }
    return $curator;
  }

  public function updateCurator($data) {
    $curator = $this->repository->updateCurator($data);
    $this->userService->updateUser($data, $curator->user);
  }

  public function deleteCurator($curatorId) {
    $this->repository->deleteCurator($curatorId);
  }
}