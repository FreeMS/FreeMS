<?php
namespace FreeMS\Services\Product;

use FreeMS\Repositories\Product\ProductRepository;

class ProductService {
  protected $repository;

  public function __construct(ProductRepository $repository){
    $this->repository = $repository;
  }

  public function getList($query = null) {
    $filter = null;
    $sort = null;

    if(isset($query['filter'])) $filter = $query['filter'];
    if(isset($query['sort'])) $sort = $query['sort'];

    $result = $this->repository->getList($filter, $sort);

    return $result;
  }

  public function getRelationData() {
    $relationData = $this->repository->getRelationData();
    return $relationData;
  }

  public function getById($productId){
    return $this->repository->getById($productId);
  }

  public function createProduct($data) {
    $this->repository->createProduct($data);
  }

  public function updateProduct($data) {
    $this->repository->updateProduct($data);
  }

  public function deleteProduct($productId) {
    return $this->repository->deleteProduct($productId);
  }
}