<?php
namespace FreeMS\Services\Brand;

use FreeMS\Repositories\Brand\BrandRepository;
use FreeMS\Services\User\UserService;

class BrandService {
  protected $repository;
  protected $userService;

  public function __construct(BrandRepository $repository, UserService $userService){
    $this->repository = $repository;
    $this->userService = $userService;
  }

  public function getList($query = null) {
    $filter = null;
    $sort = null;

    if(isset($query['filter'])) $filter = $query['filter'];
    if(isset($query['sort'])) $sort = $query['sort'];

    $result = $this->repository->getList($filter, $sort);

    return $result;
  }

  public function getById($brandId){
    return $this->repository->getById($brandId);
  }

  public function createBrand($data) {
    $userData = [
      'password' => uniqId().uniqId(),
      'name' => $data['rep_name'],
      'email' => $data['rep_email'],
      'role_key' => 'brand_representative'
    ];

    $user = $this->userService->createUser($userData);
    $this->repository->createBrand($data, $user);
  }

  public function updateBrand($data) {
    $this->repository->updateBrand($data);
  }

  public function deleteBrand($brandId) {
    return $this->repository->deleteBrand($brandId);
  }
}