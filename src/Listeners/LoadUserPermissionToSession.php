<?php

namespace FreeMS\Listeners;

use FreeMS\Models\Right;
use FreeMS\Models\Role;
use FreeMS\Models\User;
use Illuminate\Session\Store;

class LoadUserPermissionToSession
{
  private $session;

  /**
   * Create the event listener.
   *
   * @param Store $session
   */
  public function __construct(Store $session)
  {
    //
    $this->session = $session;
  }

  /**
   * Handle the event.
   *
   * @param \Illuminate\Auth\Events\Login $event
   */
  public function handle(\Illuminate\Auth\Events\Login $event)
  {
    /** @var User $user */
    $user = User::findOrFail($event->user->id);

    $user->load('roles.rights');
    $userRightOriginsMap = [];
    $user->rights->each(function (Right $right) use (&$userRightOriginsMap) {
      $userRightOriginsMap[$right->right_key] = [$right];
    });

    $user->roles->each(function (Role $role) use (&$userRightOriginsMap) {
      $role->rights->each(function (Right $right) use (&$userRightOriginsMap, $role) {
        if (!isset($userRightOriginsMap[$right->right_key])) {
          $userRightOriginsMap[$right->right_key] = [$role];
        } else {
          array_unshift($userRightOriginsMap[$right->right_key], $role);
        }
      });
    });

    $userRights = array_keys($userRightOriginsMap);

    $this->session->put('user.rights', $userRights);
  }
}
