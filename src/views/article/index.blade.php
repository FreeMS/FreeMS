@extends('freems::common.layout')

@section('content')
  <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-xs-12">
      <h2>articles</h2>
      <ol class="breadcrumb">
        <li>
          <a href="{{ route('admin.dashboard') }}">Dashboard</a>
        </li>
        <li class="active">
          <strong>articles</strong>
        </li>
      </ol>
    </div>
  </div>

  <div class="wrapper wrapper-content">

    @if(\Session::get('status'))
      <div class="row">
        <div class="col-xs-12">
          <div class="form-group">
            <div class="alert alert-success text-center">
              {{ \Session::get('status') }}
            </div>
          </div>
        </div>
      </div>
    @endif

    <div class="row">
      <div class="col-md-6">
        <a href="{{ route('admin.articles.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i>
          Create article</a>
      </div>
    </div>
    <br>

    <!-- Responsive Full Content -->
    @if(count($articles) == 0)
      <br><h3 class="text-center">Results not found</h3><br>
    @else
      <div class="row">
        <div class="col-lg-12">
          <div class="ibox float-e-margins">
            <div class="ibox-title">
              <h5>All articles</h5>
            </div>

            <div class="ibox-content">

              <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables-example">
                  <thead>
                  <tr>
                    <th>Image</th>
                    <th>Title</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($articles as $article)
                    <tr>
                      <td width="150"><img
                          src="{!! count($article->images) ? $article->images[0]->src : 'admin/img/placeholder.png' !!}"
                          alt="{!! $article->title !!}" width="150"></td>
                      <td style="vertical-align: middle">{!! $article->title !!}</td>

                      <td style="vertical-align: middle">
                        <div class="btn-group btn-group-xs">
                          <a href="{{ route('admin.articles.edit', [ $article->id ]) }}" title="Update"
                             class="btn btn-primary"><i
                              class="fa fa-pencil"></i></a>

                          <a href="javascript:void(0)" data-id="{{ $article->id }}" title="Delete"
                             class="btn btn-danger remove-item"><i
                              class="fa fa-times"></i></a>
                        </div>
                      </td>
                    </tr>
                  @endforeach
                  </tbody>
                  <tfoot>
                </table>
              </div>

            </div>
          </div>
        </div>
      </div>
    @endif

    {{ Form::open(['url' => route('admin.articles.delete', [ '' ]), 'method' => 'DELETE', 'id' => 'delete-article-form' ]) }}
    {{ Form::close() }}
  </div>
@endsection

@push('scripts')
  <script>
      $(function () {
          var indexRoute = "{{ route('freems.file.index') }}";

          window.fileManager = createFileManager({
              el: $('#file-manager-static'),
              indexRoute: indexRoute
          });

          window.fileManager.load();
      });
  </script>
@endpush