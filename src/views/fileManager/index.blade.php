@extends('admin.common.layout')

@push('styles')
  <link href="admin/css/plugins/cropper/cropper.min.css" rel="stylesheet">
@endpush
@section('content')
  <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
      <h2>File Manager</h2>
      <ol class="breadcrumb">
        <li>
          <a href="{{ route('admin.dashboard') }}">Dashboard</a>
        </li>
        <li class="active">
          <strong>File Manager</strong>
        </li>
      </ol>
    </div>
  </div>

  <div class="wrapper wrapper-content" id="file-manager-static"></div>

  @include('freems::templates.file-manager-board')

  <div class="modal inmodal fade" id="edit-image-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg file-manager-modal">
      <div class="modal-content">
        <div class="row modal-body">
          <div class="ibox float-e-margins">
            <div class="ibox-title  back-change">
              <h5>Crop image</h5>
              <div class="ibox-tools">
                <a class="collapse-link">
                  <i class="fa fa-chevron-up"></i>
                </a>
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                  <i class="fa fa-wrench"></i>
                </a>
                <a data-dismiss="modal">
                  <i class="fa fa-times"></i>
                </a>
              </div>
            </div>
            <div class="ibox-content">
              <div class="row">
                <div class="col-md-6">
                  <div class="image-crop">
                    <img id="crop-image" src="">
                  </div>
                </div>
                <div class="col-md-6">
                  <h4>Preview image</h4>
                  <div class="img-preview img-preview-sm"></div>
                  <br><br>

                  <div class="btn-group">
                    <button class="btn btn-white" id="zoomIn" type="button">Zoom In</button>
                    <button class="btn btn-white" id="zoomOut" type="button">Zoom Out</button>
                    <button class="btn btn-white" id="rotateLeft" type="button">Rotate Left</button>
                    <button class="btn btn-white" id="rotateRight" type="button">Rotate Right</button>

                    <button class="btn btn-warning" id="initCrop" type="button">Crop</button>
                    <button class="btn btn-primary image-crop-btn" data-dismiss="modal" type="button">Done</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('scripts')


  <script>
    $(function () {
      var indexRoute = "{{ route('freems.file.index') }}";

      window.fileManager = createFileManager({
        el: $('#file-manager-static'),
        indexRoute: indexRoute
      });

      window.fileManager.load();
    });
  </script>
@endpush