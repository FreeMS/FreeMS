<?php
$user = Auth::user();
?>
  <!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>Settr | Admin</title>
  <base href="{{ URL::to('/') }}/">

  <link href="vendor/freems/css/bootstrap.min.css" rel="stylesheet">
  <link href="vendor/freems/font-awesome/css/font-awesome.css" rel="stylesheet">
  <link href="vendor/freems/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
  <link href="vendor/freems/css/plugins/steps/jquery.steps.css" rel="stylesheet">

  <!-- Toastr style -->
  <link href="vendor/freems/css/plugins/toastr/toastr.min.css" rel="stylesheet">

  <!-- Gritter -->
  <link href="vendor/freems/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

  <link href="vendor/freems/css/animate.css" rel="stylesheet">

  @stack('styles')

  <link href="vendor/freems/css/style.css" rel="stylesheet">
</head>

<body>
<div id="wrapper">
  <nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
      <ul class="nav metismenu" id="side-menu">
        <li class="nav-header">
          <div class="dropdown profile-element">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="clear"> <span class="block m-t-xs"> <strong
                    class="font-bold">{{ $user->name }}</strong></span></span></a>
          </div>
          <div class="logo-element">SETTR</div>
        </li>

        @yield('admin-menu')
      </ul>

    </div>
  </nav>

  <div id="page-wrapper" class="gray-bg dashbard-1">
    <div class="row border-bottom">
      <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
          <a style="margin-top: 15px; margin-right: 20px;" class="navbar-minimalize minimalize-styl-2 btn btn-primary"
             href="javascript:;"><i class="fa fa-bars"></i> </a>

          <h2 style="margin-top: 15px; font-weight: bold; color: #39bf9e; white-space: nowrap;"> Management Panel</h2>

          <form role="search" class="navbar-form-custom hidden" action="javascript:;">
            <div class="form-group">
              <input type="text" placeholder="Search for something..." class="form-control" name="top-search"
                     id="top-search">

            </div>
          </form>
        </div>
        <ul class="nav navbar-top-links navbar-right">
          <li>
            <span class="m-r-sm text-muted welcome-message">Welcome</span>
          </li>

          @if(config('app.locales'))
          <li class="dropdown">
            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="javascript:;">
              <img src="vendor/freems/img/flags/{{ \App::getLocale() }}.png"
                   alt="{{ config('app.locale_descriptions')[\App::getLocale()] }}">
            </a>
            <ul class="dropdown-menu dropdown-alerts" style="width: 150px">
              @foreach(config('app.locales') as $locale)
                @if($locale == \App::getLocale()) @continue @endif
              <li>
                <a href="{{ str_replace('/'.\App::getLocale().'/', '/'.$locale.'/', Request::url()) }}"><img src="vendor/freems/img/flags/{{ $locale }}.png" alt="{{ config('app.locale_descriptions')[$locale] }}"> <span class="lang-caption">{{ config('app.locale_descriptions')[$locale] }}</span></a>
              </li>
                @if(!$loop->last)
                  <li class="divider"></li>
                @endif
              @endforeach
            </ul>
          </li>
          @endif


          <li>
            <a href="{{ route('sign-out') }}"><i class="fa fa-sign-out"></i> Log out</a>
          </li>
        </ul>

      </nav>
    </div>

    @yield('content')

  </div>
</div>

<!-- Mainly scripts -->
<script src="vendor/freems/js/jquery-3.3.1.min.js"></script>
<script src="vendor/freems/js/bootstrap.min.js"></script>
<script src="vendor/freems/js/bootstrap-confirmation.min.js"></script>
<script src="vendor/freems/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="vendor/freems/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="vendor/freems/js/inspinia.js"></script>
<script src="vendor/freems/js/plugins/pace/pace.min.js"></script>

<!-- Custom Modal class using bootstrap modals -->
<script src="vendor/freems/js/Modal.js"></script>

<script src="vendor/freems/js/custom/common.js"></script>

<script>
  var _token = "{{ csrf_token() }}";

  $(function () {
    $('.logout-link').click(function () {
      Modal.show({
        yesClass: 'btn-danger',
        body: 'Are you sure, you want to logout from the system?',
        yes: 'Log out',
        callback: function (btn) {
          Modal.hide();

          if (btn === 'yes') {
            $('#logout-form').submit();
          }
        }
      });
    });
  });
</script>

<!-- Other scrips for specific pages -->
@stack('scripts')

</body>
</html>
