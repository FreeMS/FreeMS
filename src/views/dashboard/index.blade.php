@extends('admin.common.layout')

<?php $user = \Auth::user(); ?>

@section('content')
<div class="row  border-bottom white-bg dashboard-header">
  <div class="col-md-12">
    <h2>Welcome {{ $user->name }}</h2>
  </div>
</div>
@endsection