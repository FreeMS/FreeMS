<div class="file-box file-box-image template">
  <div class="file">
    <span class="corner"></span>
    <div class="image">
      <div class="transparent"></div>
      <img alt="image" class="img-responsive" src="">
    </div>
    <div class="file-name">
      <span data-prop="title"></span>
      <br/>
      <small>Added: <span data-prop="created_at"></span></small>
    </div>
  </div>
</div>