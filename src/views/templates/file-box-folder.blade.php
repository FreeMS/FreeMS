<div class="file-box file-box-folder template">
  <div class="file">
    <div class="open-handler">
      <span class="corner"></span>
      <div class="icon">
        <i class="fa fa-folder"></i>
      </div>
      <div class="file-name text-center">
        <span data-prop="name"></span>
      </div>
    </div>
  </div>
</div>