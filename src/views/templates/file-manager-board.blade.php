@push('styles')
<link href="vendor/freems/css/plugins/fancybox/jquery.fancybox.min.css" rel="stylesheet">
<link href="admin/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
<link href="admin/css/plugins/iCheck/custom.css" rel="stylesheet">
<style>
  .sortable-images {
    list-style-type: none;
    margin: 0;
    padding: 0;
    width: 100%;
  }

  .sortable-images li {
    cursor: pointer;
    margin: 3px 3px 3px 0;
    padding: 1px;
    float: left;
  }
</style>
@endpush

<div class="file-manager-board-template">

  <div class="row file-manager-wrapper">
    <div class="col-lg-3">
      <div class="ibox float-e-margins">
        <div class="ibox-content">
          <div class="file-manager">
            <h5>ფილტრი:</h5>
            <div class="type-filter-container">
              <a href="javascript:;" class="file-control active" data-type="all">All</a>
            </div>

            <div class="hr-line-dashed"></div>

            <div class="btn-group btn-block">
              <label title="Upload image file" class="btn btn-primary btn-block">
                <i class="fa fa-plus-square"></i> <input type="file" name="files[]" multiple class="hide file-manager-input"> აირჩიეთ ფაილ(ებ)ი
              </label>
            </div>

            <div class="btn-group btn-block">
              <button class="btn  btn-block open-video-form bg-info"><i class="fa fa-plus-square"></i>  ვიდეოს დამატება</button>
            </div>

            <div class="btn-group btn-block">
              <button class="btn btn-block create-folder-btn bg-warning"><i class="fa fa-plus-square"></i> ფოლდერის შექმნა</button>
            </div>

            <br>
            <br>
            <form action="javascript:;" class="col-lg-12 file-manager-image-form file-manager-form-no-styles"
                  style="display: none">
              <div class="image">
                <img alt="image" class="img-responsive" data-toggle="modal" src="">
                <input type="hidden" name="crop" value="">
              </div>
              <br>
              <div class="row">
                <div class="col-xs-12">
                  <div class="form-group">
                    <label>Title:</label>
                    <input class="form-control" value="file title" name="title">
                  </div>
                </div>

                <div class="col-xs-12">
                  <div class="form-group">
                    <label>Caption:</label>
                    <input class="form-control" value="Caption" name="caption">
                  </div>
                </div>

                <div class="col-xs-12">
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary save-file-btn">შენახვა</button>
                  </div>
                </div>
              </div>
            </form>

            <form action="javascript:;" class="col-lg-12 file-manager-video-form file-manager-form-no-styles"
                  style="display: none">
              <label class="btn-block" title="Click and change video poster">
                <div class="image">
                  <img alt="image" class="img-responsive" src="vendor/freems/img/placeholder.png">
                </div>
                <input type="file" name="files[]" class="hide file-manager-video-input">
              </label>
              <br>
              <div class="row">
                <div class="col-xs-12">
                  <div class="form-group">
                    <label>Video link:</label>
                    <input class="form-control video_link_input" autocomplete="off" name="video_link">
                    <input type="hidden" name="type" class="hide video-type-input">
                    <input type="hidden" name="video_id" class="hide video-id-input">
                  </div>
                </div>

                <div class="col-xs-12">
                  <div class="form-group">
                    <button type="submit" class="btn save-file-btn bg-info">შენახვა</button>
                  </div>
                </div>
              </div>
            </form>

            <form action="javascript:;" class="col-lg-12 file-manager-folder-form file-manager-form-no-styles"
                  style="display: none">
              <label class="btn-block text-center">
                <div class="icon">
                  <i class="fa fa-folder" style="font-size: 70px"></i>
                </div>
              </label>
              <br>
              <div class="row">
                <div class="col-xs-12">
                  <div class="form-group">
                    <label>დასახელება:</label>
                    <input class="form-control" name="name" autocomplete="off">
                  </div>
                </div>

                <div class="col-xs-12">
                  <div class="form-group">
                    <button type="submit" class="btn save-folder-btn bg-warning">შენახვა</button>
                  </div>
                </div>
              </div>
            </form>

            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-9 file-manager-area list-view animated fadeInRight">
      <div class="row">
        <div class="col-lg-12">
          <div class="page-haeding" style="padding: 15px 30px; background: #fff;">
            <div class="row">
              <div class="col-md-8">
                <ol class="breadcrumb">
                  <li class="active">ყველა</li>
                </ol>
              </div>

              <div class="col-md-4 text-right">
                <label class="change-view">
                  <i class="fas fa-th-large"></i>
                  <i class="fas fa-list-ul"></i>
                </label>

                <label><div class="i-checks"><label> <input type="checkbox" name="show_hidden_files" value="1" > <i></i> &nbsp;&nbsp; დამალული ფაილები </label></div></label>
                &nbsp;
                <button class="btn btn-primary btn-xs reload-current-folder-btn"><i class="fas fa-sync-alt"></i></button>
              </div>
            </div>
          </div>
          <br>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-12 file-manager-container"></div>
      </div>
    </div>
  </div>
</div>

@include('freems::templates.file-box-folder')
@include('freems::templates.file-box-image')
@include('freems::templates.file-box-file')

@push('scripts')
  <!-- little hack to make contextmenu direct child of body -->
  @include('freems::templates.file-manager-contextmenu')
  <script src="admin/js/plugins/iCheck/icheck.min.js"></script>
  <script src="vendor/freems/js/plugins/cropper/cropper.min.js"></script>
  <script src="vendor/freems/js/plugins/fancybox/jquery.fancybox.min.js"></script>
  <script src="vendor/freems/js/plugins/jquery-ui/jquery-ui.js"></script>
  <script src="vendor/freems/js/custom/file-manager.js"></script>
@endpush