<div class="modal inmodal fade file-manager-modal-template" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg file-manager-modal">
    <div class="modal-content">
      <div class="modal-header" style="padding: 5px 15px;">
        <h2>ატვირთული ფაილები</h2>
      </div>

      <div class="modal-body"></div>

      <div class="modal-footer">
        <button class="btn btn-default" data-dismiss="modal">დახურვა</button>
        <button class="btn btn-primary submit-modal-btn">მიმაგრება</button>
      </div>
    </div>
  </div>
</div>

@include('freems::templates.file-manager-board')