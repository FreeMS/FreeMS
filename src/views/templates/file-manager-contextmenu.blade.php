<div class="file-manager-contextmenu files-contextmenu">
  <a class="download-file"> <i class="fa fa-download"></i> <span>გადმოწერა</span></a>
  <a class="download-original"> <i class="fa fa-download"></i> <span>ორიგინალის გადმოწერა</span></a>
  <a class="remove-watermark"> <i class="fa fa-download"></i> <span>Watermark-ის წაშლა</span></a>
  <a class="edit-file"><i class="fas fa-pencil-alt"></i> <span>რედაქტირება</span></a>
  <a class="delete-file"><i class="fa fa-trash"></i> <span>წაშლა</span></a>
  <a class="restore-file"><i class="fas fa-share-square"></i> <span>აღდგენა</span></a>
  <a class="delete-forever"><i class="fa fa-trash"></i> <span>სამუდამოდ წაშლა</span></a>
</div>
<div class="file-manager-contextmenu folders-contextmenu">
  <a class="download-original"> <i class="fa fa-download"></i> <span>ორიგინალის გადმოწერა</span></a>
  <a class="delete-file"><i class="fa fa-trash"></i> <span>წაშლა</span></a>
  <a class="restore-file"><i class="fas fa-share-square"></i> <span>აღდგენა</span></a>
  <a class="delete-forever"><i class="fa fa-trash"></i> <span>სამუდამოდ წაშლა</span></a>
</div>