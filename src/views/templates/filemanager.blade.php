<script>
  var imageGroup = null;

  $(function () {
    var fileManager = createFileManager({
      modal: true,
      fileType: {!! @$anyFileType ? 'undefined' : '"image"' !!},
      folderId: "{{ @$folderId }}",
      indexRoute: "{{ route('freems.file.index') }}",
      uploadBehaviour: 'related',
      onSelect: function (files) {
        var data = imageGroup.data() || {};
        var name = data.name || 'images[]';
        var multi = !(data.multi === false);

        if (files.length > 0) {
          imageGroup.find('.image-previews').parents('.form-group').show();
        }

        if(!multi) {
          files = files.slice(0, 1);
        }

        $.each(files, function (k, file) {
          if(imageGroup.find('.image-previews li[data-id="' + file.id + '"]').length) return; // Already exists

          if(!multi) {
            imageGroup.find('.selected-image-inputs input').remove();
            imageGroup.find('.image-previews li').remove();
          }

          imageGroup.find('.selected-image-inputs').append(
            $('<input>').attr({type: 'hidden', name: name, value: file.id})
          );

          var previewImage = file.type === 'image' ?
            $('<img>').attr({src: file.src, alt: file.title, href: file.src, 'data-fancybox': 'gallery'}) :
            $('<a>').attr({ href: file.src, target: '_blank' }).addClass('icon').data({ 'src': file.src }).html('<i class="fa fa-file"></i>');

          imageGroup.find('.image-previews').append(
            $('<li>').attr({
              href: file.src,
              title: file.title,
              'data-id': file.id,
            }).addClass("image").append(
              $('<button>').addClass('btn btn-danger btn-circle').attr({
                'data-id': file.id,
                type: 'button'
              }).append($('<i>').addClass('fa fa-times')),
              previewImage
            ));
        });

        bindEventsOnRemove();
        bindFancyboxEvenets();
        bindSortableEvents(true);
      }
    });

    $('.select-media-btn').click(function () {
      imageGroup = $(this).closest('.image-group');
      fileManager.show();
    });

    function bindFancyboxEvenets() {
      $('.image-previews li img').fancybox();
    }

    function bindSortableEvents(rebind) {
      if (rebind) $(".sortable-images").sortable('destroy');
      $(".sortable-images").sortable({
        handle: 'img',
        cancel: '',
        update: function (event, ui) {
          var scopeEl = $(this).closest('.image-group');
          var name = scopeEl.data().name || 'images[]';

          var imageInputs = scopeEl.find("input[name='" + name + "']");
          var imageInputsById = {};
          $.each(imageInputs, function (k, input) {
            imageInputsById[input.value] = input;
            $(input).remove();
          });

          var sortedIds = $(this).closest(".sortable-images").sortable("toArray", {attribute: 'data-id'});
          $.each(sortedIds, function (k, imageId) {
            scopeEl.find('.selected-image-inputs').append($(imageInputsById[imageId]));
          });
        }
      });
    }

    function bindEventsOnRemove() {
      $('.image-previews .image button').click(function (e) {
        var scopeEl = $(this).closest('.image-group');

        e.stopPropagation();
        e.preventDefault();
        var id = $(this).data().id;
        var parent = $(this).parent();

        Modal.show({
          yesClass: 'btn-danger',
          body: 'Are you sure, you want to Remove file?',
          yes: 'Yes',
          callback: function (btn) {
            Modal.hide();

            if (btn === 'yes') {
              scopeEl.find('.selected-image-inputs input[value="' + id + '"]').remove();
              parent.remove();
              if (scopeEl.find('.image-previews li').length < 1) {
                scopeEl.find('.image-previews').parents('.form-group').hide();
              }
            }
          }
        });
      })
    }

    bindFancyboxEvenets();
    bindEventsOnRemove();
    bindSortableEvents();
  })
</script>
