<div class="file-box file-box-file template">
  <div class="file">
    <span class="corner"></span>
    <div class="icon">
      <i class="fa fa-file"></i>
    </div>
    <div class="file-name">
      <span data-prop="title"></span>
      <br/>
      <small>Added: <span data-prop="created_at"></span></small>
    </div>
  </div>
</div>