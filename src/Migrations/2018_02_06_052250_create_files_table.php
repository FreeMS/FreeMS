<?php

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('files', function (Blueprint $table) {
      $table->increments('id');

      $table->string('src');
      $table->text('title')->nullable();
      $table->integer('width')->nullable();
      $table->integer('height')->nullable();
      $table->string('type')->nullable();
      $table->string('extension')->nullable();
      $table->text('caption')->nullable();
      $table->string('video_id')->nullable();
      $table->string('slug')->unique();

      $table->timestamps();
      $table->softDeletes();
    });

    Schema::create('thumbnails', function (Blueprint $table) {
      $table->increments('id');
      $table->text('title')->nullable();
      $table->string('slug')->unique();
      $table->string('src');
      $table->integer('width')->nullable();
      $table->integer('height')->nullable();
      $table->unsignedInteger('file_id');

      $table->foreign('file_id')->references('id')->on('files')->onDelete('cascade');

      $table->softDeletes();
    });

    Schema::create('fileables', function (Blueprint $table) {
      $table->unsignedInteger('file_id');
      $table->unsignedInteger('fileable_id');
      $table->unsignedInteger('ord')->default(0);
      $table->string('fileable_type');
      $table->primary(['file_id', 'fileable_id', 'fileable_type']);

      $table->foreign('file_id')->references('id')->on('files')->onDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('fileables');
    Schema::dropIfExists('thumbnails');
    Schema::dropIfExists('files');

    $file = new Filesystem();
    $file->cleanDirectory(public_path('storage/files'));
    $file->cleanDirectory(public_path('storage/thumbnails'));
  }
}
